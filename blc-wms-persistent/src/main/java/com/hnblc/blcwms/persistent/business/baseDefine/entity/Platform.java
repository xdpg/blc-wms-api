package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFPLATFORM")
public class Platform implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电商平台代码
     */
    @TableId("COMPANY_CODE")
    private String companyCode;

    /**
     * 电商平台名称
     */
    @TableField("COMPANY_NAME")
    private String companyName;

    /**
     * 联系人电话
     */
    @TableField("CONTACT_PHONE")
    private String contactPhone;

    /**
     * 通讯地址
     */
    @TableField("CONTACT_ADDRESS")
    private String contactAddress;

    /**
     * 联系人邮箱
     */
    @TableField("CONTACT_EMAIL")
    private String contactEmail;

    /**
     * 联系人姓名
     */
    @TableField("CONTACT_MAN")
    private String contactMan;

    /**
     * 是否跨境
     */
    @TableField("IS_CROSS_BORDER")
    private String isCrossBorder;


}
