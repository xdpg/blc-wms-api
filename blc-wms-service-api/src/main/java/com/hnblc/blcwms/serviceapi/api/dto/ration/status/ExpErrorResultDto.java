package com.hnblc.blcwms.serviceapi.api.dto.ration.status;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @ClassName: ExpErrorResultDto
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/11/1
 * @Version: V1.0
 */

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ExpErrorResultDto {
    private String orderNo;
    private String errorCode;
    private String errorDetail;
}
