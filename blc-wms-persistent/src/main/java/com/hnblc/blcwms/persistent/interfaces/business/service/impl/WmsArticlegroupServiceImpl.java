package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsArticlegroup;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsArticlegroupMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsArticlegroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-18
 */
@Service
public class WmsArticlegroupServiceImpl extends ServiceImpl<WmsArticlegroupMapper, WmsArticlegroup> implements IWmsArticlegroupService {

}
