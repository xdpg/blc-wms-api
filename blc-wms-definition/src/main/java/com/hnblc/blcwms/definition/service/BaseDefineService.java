package com.hnblc.blcwms.definition.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnblc.blcwms.common.constant.CommonConst;
import com.hnblc.blcwms.persistent.business.basedefine.entity.*;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.WmsBaseMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.IArticleService;
import com.hnblc.blcwms.persistent.business.basedefine.service.IOwnerService;
import com.hnblc.blcwms.persistent.business.basedefine.service.IShipperService;
import com.hnblc.blcwms.serviceapi.definition.IBaseDefineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BaseDefineService implements IBaseDefineService {


    @Autowired
    IOwnerService ownerService;
    @Autowired
    IShipperService shipperService;
    @Autowired
    IArticleService articleService;

    @Autowired
    WmsBaseMapper wmsBaseMapper;

    @Override
    public void getSheetNo(Map<String,Object> generateSheetMap) {
        wmsBaseMapper.getSheetNo(generateSheetMap);
    }
    @Override
    public void initUpdateSku(Map<String,Object> generateSheetMap) {
        wmsBaseMapper.initUpdateSku(generateSheetMap);
    }
    @Override
    public void initStatus(Map<String,Object> generateSheetMap) {
        wmsBaseMapper.initStatus(generateSheetMap);
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_BUSINESS_DEFINITION_SHIPPER,key = "#warehouseNo.concat('::').concat(#shipperNo)")
    public Shipper getShipperCashe(String warehouseNo, String shipperNo) {
        return shipperService.getOne(new QueryWrapper<Shipper>().eq("warehouse_no",warehouseNo)
                .eq("shipper_no",shipperNo));
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_BUSINESS_DEFINITION_CUSTOM,key = "#enterpriseNo.concat('::').concat(#ownerNo).concat('::').concat(#custNo)")
    public Cust getCustCashe(String enterpriseNo,String ownerNo, String custNo) {
        return null;
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_BUSINESS_DEFINITION_SUPPLIER,key = "#ownerNo.concat('::').concat(#supplierNo)")
    public Supplier getSupplierCashe(String ownerNo, String supplierNo) {
        return null;
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_BUSINESS_DEFINITION_SHIPPER,key = "#ownerNo")
    public Owner getOwnerCache(String ownerNo) {
        return ownerService.getOne(new QueryWrapper<Owner>().eq("owner_no",ownerNo));
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_SYSTEM_DICTIONARIES_MAP,key = "#ownerNo.concat('::').concat(#ownerArticleNo)")
    public Article getArticleCache(String ownerNo, String ownerArticleNo ){

        return articleService.getOne(new QueryWrapper<Article>().eq("owner_no",ownerNo)
        .eq("owner_article_no",ownerArticleNo));
    }
}
