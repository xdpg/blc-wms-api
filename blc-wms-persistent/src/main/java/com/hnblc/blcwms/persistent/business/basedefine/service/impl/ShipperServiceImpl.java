package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Shipper;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.ShipperMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.IShipperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class ShipperServiceImpl extends ServiceImpl<ShipperMapper, Shipper> implements IShipperService {

}
