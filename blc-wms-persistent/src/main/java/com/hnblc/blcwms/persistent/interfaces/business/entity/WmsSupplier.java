package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_SUPPLIER")
public class WmsSupplier implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 传单通讯使用(建议使用自动增加的系列号)
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 供货商编码
     */
    @TableField("SUPPLIER_NO")
    private String supplierNo;

    /**
     * 供货商名称
     */
    @TableField("SUPPLIER_NAME")
    private String supplierName;

    /**
     * 供货商负责人名称
     */
    @TableField("MANAGER")
    private String manager;

    /**
     * 供货商联络人名称
     */
    @TableField("LINKMAN")
    private String linkman;

    /**
     * 供货商联络地址
     */
    @TableField("ADDRESS")
    private String address;

    /**
     * 供货商邮编
     */
    @TableField("ZIPCODE")
    private String zipcode;

    /**
     * 联络电话
     */
    @TableField("PHONE")
    private String phone;

    /**
     * 传真号码
     */
    @TableField("FAX")
    private String fax;

    /**
     * 联络人E-Mail
     */
    @TableField("E_MAIL")
    private String eMail;

    /**
     * 0表示停止往来；1表示正常
     */
    @TableField("STATUS")
    private Double status;

    /**
     * 说明与备注
     */
    @TableField("NOTES")
    private String notes;

    /**
     * 企业编码：无需插入值，仅保留默认值即可
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 委托业主编码：无需插入值，仅保留默认值即可
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 接收时间
     */
    @TableField("HANDLETIME")
    private LocalDateTime handletime;


}
