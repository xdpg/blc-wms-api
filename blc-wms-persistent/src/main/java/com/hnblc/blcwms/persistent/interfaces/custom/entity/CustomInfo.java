package com.hnblc.blcwms.persistent.interfaces.custom.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客户在传单过程中是否有自定义收件人等信息
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_C_CUSTOM_INFO")
public class CustomInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 销售客户代码
     */
    @TableId("CUSTOM_NO")
    private String customNo;

    /**
     * 海关备案企业代码
     */
    @TableField("EBC_CODE")
    private String ebcCode;

    /**
     * 销售客户类型，1.普通电商，2.跨境电商
     */
    @TableField("CUSTOM_TYPE")
    private String customType;

    /**
     * 客户默认发货人
     */
    @TableField("CUSTOM_SEND_NAME")
    private String customSendName;

    /**
     * 客户默认发货人电话
     */
    @TableField("CUSTOM_SEND_PHONE")
    private String customSendPhone;

    /**
     * 客户默认发货人地址
     */
    @TableField("CUSTOM_SEND_ADDRESS")
    private String customSendAddress;


}
