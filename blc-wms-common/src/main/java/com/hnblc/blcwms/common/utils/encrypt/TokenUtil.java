package com.hnblc.blcwms.common.utils.encrypt;

import com.hnblc.blcwms.common.constant.StringPool;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.TreeMap;

public class TokenUtil {

    /**
     * 生成token
     * @param timeStamp
     * @param seed
     * @return
     * @throws Exception
     */
    public static String generateToken(String timeStamp, TreeMap<String,String> seed,String encryptKey) throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<String> iterator = seed.keySet().iterator();
        while(iterator.hasNext()){
            final String key = iterator.next();
            stringBuilder.append(seed.get(key))
            .append(StringPool.COMMON_SEPARATOR_SPLIT_COLON)
            .append(seed.get(key)).append(StringPool.COMMON_SEPARATOR_SPLIT_COMMA);
        }
        return MD5Util.md5Encode(SHAUtil.shaEncode(stringBuilder.toString().toUpperCase())+timeStamp+encryptKey.toUpperCase());
    }

    public static boolean verifyToken(String timeStamp,TreeMap<String,String> seed,String encryptKey,String token) throws UnsupportedEncodingException {
        return generateToken(timeStamp,seed,encryptKey).equalsIgnoreCase(token);
    }
}
