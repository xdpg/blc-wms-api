package com.hnblc.blcwms.rest.config;

import com.hnblc.blcwms.rest.config.properties.PinduoduoProperties;
import com.pdd.pop.sdk.http.PopAccessTokenClient;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PinduoduoConfig {

    @Bean(name="pddProperties")
    PinduoduoProperties initPinduoduoProperties(){
        return new PinduoduoProperties();
    }

    @Bean
    PopClient pinduoduoClient(@Qualifier("pddProperties") PinduoduoProperties pinduoduoConfig){
        return new PopHttpClient(pinduoduoConfig.getClientId(),pinduoduoConfig.getClientSecret());
    }

    @Bean
    PopAccessTokenClient pinduoduoTokenClient(@Qualifier("pddProperties") PinduoduoProperties pinduoduoConfig){
        return new PopAccessTokenClient(pinduoduoConfig.getClientId(),pinduoduoConfig.getClientSecret());
    }
}
