package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFLOC")
public class Loc implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    @TableField("WAREHOUSE_NAME")
    private String warehouseName;

    @TableField("MEMO")
    private String memo;

    @TableField("CREATE_FLAG")
    private String createFlag;

    @TableField("RGST_NAME")
    private String rgstName;

    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    @TableField("UPDT_NAME")
    private String updtName;

    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    @TableField("ADRESS")
    private String adress;

    @TableField("LINKMAN")
    private String linkman;

    @TableField("TEL")
    private String tel;

    @TableField("MANAGE_NAME")
    private String manageName;

    @TableField("PROVINCE")
    private String province;

    @TableField("CITY")
    private String city;

    @TableField("ZONE")
    private String zone;

    @TableField("COMPANY_NO")
    private String companyNo;

    @TableField("SCAN_FLAG")
    private String scanFlag;

    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("ROW_ID")
    private Long rowId;


}
