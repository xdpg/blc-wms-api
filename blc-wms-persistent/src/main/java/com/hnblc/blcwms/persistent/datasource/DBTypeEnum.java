package com.hnblc.blcwms.persistent.datasource;

public enum DBTypeEnum {
    apiDB("apiDB"), businessDB("businessDB"),ecssentDB("ecssentDB");
    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
