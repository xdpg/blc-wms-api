package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_RATIONNOTE")
public class WmsRationnote implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发货通知单号
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 发货类型：0：普通出货（默认），1：直通出货
     */
    @TableField("RTYPE")
    private Double rtype;

    /**
     * 商品编码
     */
    @TableField("GOODSID")
    private String goodsid;

    /**
     * 委托业主编码
     */
    @TableField("CUSTOMID")
    private String customid;

    /**
     * 仓库编码
     */
    @TableField("PALLETZONE")
    private String palletzone;

    /**
     * 是否紧急单：一般单=’0’、紧急单=’1’默认值一般单（紧急出货单）
     */
    @TableField("TYPE")
    private String type;

    /**
     * 门店代码
     */
    @TableField("SHOPID")
    private String shopid;

    /**
     * 原订单号码（直通时，为进货单单号）
     */
    @TableField("REFSHEETID")
    private String refsheetid;

    /**
     * 配量优先级(1-999)默认值”100”
     */
    @TableField("LEVELTYPE")
    private String leveltype;

    /**
     * 出货日期：本单据预定出货日期,没有日期时，默认为当天。
     */
    @TableField("SDATE")
    private LocalDateTime sdate;

    /**
     * 单内序号
     */
    @TableField("SERIALID")
    private Double serialid;

    /**
     * 出货单价
     */
    @TableField("COST")
    private Double cost;

    /**
     * 预定出货数量
     */
    @TableField("PLANQTY")
    private Double planqty;

    /**
     * 说明
     */
    @TableField("NOTES")
    private String notes;

    /**
     * 包装数量
     */
    @TableField("PKCOUNT")
    private Double pkcount;

    /**
     * ERP的机构代码
     */
    @TableField("SHOP_NO")
    private String shopNo;

    @TableField("ORDERUNIT")
    private String orderunit;

    @TableField("ORDERQUANTITY")
    private Double orderquantity;

    @TableField("UNITRATE")
    private Double unitrate;

    /**
     * 集货提单号

     */
    @TableField("PO_NO")
    private String poNo;

    /**
     * 集货类型：0：进口；1：出口；

     */
    @TableField("JH_TYPE")
    private String jhType;

    /**
     * 承运商：1、顺丰；2、跨越速运；3、申通快递；4、圆通快递；5、申通；6、中通；7、韵达；8、邮政EMS；9、天天；

     */
    @TableField("SHIPPER_NO")
    private String shipperNo;

    /**
     * 快递单号

     */
    @TableField("SHIPPER_DELIVER_NO")
    private String shipperDeliverNo;

    /**
     * 订单来源。（1、京东；2、1号店；3、天猫；0，其他）

     */
    @TableField("ORDER_SOURCE")
    private String orderSource;

    /**
     * 字符预留字段,【保税布控状态；0：不布控；1：布控】

     */
    @TableField("RSV_VAROD3")
    private String rsvVarod3;

    /**
     * 发货人地址

     */
    @TableField("SEND_ADDRESS")
    private String sendAddress;

    /**
     * 发货人

     */
    @TableField("SEND_NAME")
    private String sendName;

    /**
     * 发货人公司名称

     */
    @TableField("SEND_COMPANY_NAME")
    private String sendCompanyName;

    /**
     * 发货人邮编

     */
    @TableField("SEND_POSTCODE")
    private String sendPostcode;

    /**
     * 发货人手机

     */
    @TableField("SEND_MOBILE_PHONE")
    private String sendMobilePhone;

    /**
     * 发货人固定电话

     */
    @TableField("SEND_TELEPHONE")
    private String sendTelephone;

    /**
     * 发货省份

     */
    @TableField("SEND_PROVINCE")
    private String sendProvince;

    /**
     * 发货市

     */
    @TableField("SEND_CITY")
    private String sendCity;

    /**
     * 发货区

     */
    @TableField("SEND_ZONE")
    private String sendZone;

    /**
     * 发货村镇

     */
    @TableField("SEND_COUNTRY")
    private String sendCountry;

    /**
     * 收货人地址

     */
    @TableField("CUST_ADDRESS")
    private String custAddress;

    /**
     * 收货人

     */
    @TableField("CONTACTOR_NAME")
    private String contactorName;

    /**
     * 收货人电话

     */
    @TableField("CUST_PHONE")
    private String custPhone;

    /**
     * 收货人邮箱

     */
    @TableField("CUST_MAIL")
    private String custMail;

    /**
     * 收货省份

     */
    @TableField("RECEIVE_PROVINCE")
    private String receiveProvince;

    /**
     * 收货市

     */
    @TableField("RECEIVE_CITY")
    private String receiveCity;

    /**
     * 收货区

     */
    @TableField("RECEIVE_ZONE")
    private String receiveZone;

    /**
     * 收货村镇

     */
    @TableField("RECEIVE_COUNTRY")
    private String receiveCountry;

    /**
     * 1:正常；0：撤销

     */
    @TableField("STATUS")
    private String status;

    /**
     * 1：自提；0；普通

     */
    @TableField("ZT_FLAG")
    private String ztFlag;

    /**
     * 订单批次

     */
    @TableField("ORDER_BATCH")
    private String orderBatch;

    /**
     * 订单商品类型：0-普通；1-赠品

     */
    @TableField("ITEM_TYPE")
    private String itemType;

    /**
     * 商品备案号
     */
    @TableField("ARTICLE_IDENTIFIER")
    private String articleIdentifier;

    /**
     * 商品主条码
     */
    @TableField("BARCODE")
    private String barcode;

    /**
     * 面单打印地址码
     */
    @TableField("DELIVER_ADDRESS")
    private String deliverAddress;

    /**
     * 订单下发处理结果
     */
    @TableField("PUSHRESULT")
    private String pushresult;


}
