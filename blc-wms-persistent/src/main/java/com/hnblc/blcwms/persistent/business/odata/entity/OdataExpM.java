package com.hnblc.blcwms.persistent.business.odata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ODATA_EXP_M")
public class OdataExpM implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 出货单别
     */
    @TableField("EXP_TYPE")
    private String expType;

    /**
     * 仓别
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    /**
     * 出货单号
     */
    @TableId(type= IdType.INPUT)
    @TableField("EXP_NO")
    private String expNo;

    /**
     * 货主
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 货主客户编码
     */
    @TableField("OWNER_CUST_NO")
    private String ownerCustNo;

    /**
     * 客户编码
     */
    @TableField("CUST_NO")
    private String custNo;

    /**
     * 子客户编码
     */
    @TableField("SUB_CUST_NO")
    private String subCustNo;

    /**
     * 订单出货类型：OE:普通配送；OF:市内配送；OG:市外配送；OW:批发出货；OH：行政调拨；ON:仓间调拨；OJ:自提；OK:补单；OM:自提备货；OR:绿色通道订单。
     */
    @TableField("SOURCEEXP_TYPE")
    private String sourceexpType;

    /**
     * 订单号(朝批用于存放配送单号)
     */
    @TableField("SOURCEEXP_NO")
    private String sourceexpNo;

    /**
     * 出货日期（大表分区使用的字段）
     */
    @TableField("EXP_DATE")
    private LocalDateTime expDate;

    /**
     * 0：普通单;1：紧急单
     */
    @TableField("FAST_FLAG")
    private String fastFlag;

    /**
     * 状态
     */
    @TableField("STATUS")
    private String status;

    /**
     * 订单优先级
     */
    @TableField("PRIORITY")
    private Integer priority;

    /**
     * 补充订单原WMS单号
     */
    @TableField("ADD_EXP_NO")
    private String addExpNo;

    /**
     * 进货单号
     */
    @TableField("IMPORT_NO")
    private String importNo;

    /**
     * 配送方式 1：自提；2：配送；3：宅配；4：第三方配送；5：集货点
     */
    @TableField("DELIVER_TYPE")
    private String deliverType;

    /**
     * 运输方式 1：公路运输；2：铁路运输；3：水运；4：空运
     */
    @TableField("TRANSPORT_TYPE")
    private String transportType;

    /**
     * 批次号
     */
    @TableField("BATCH_NO")
    private String batchNo;

    /**
     * 线路编码
     */
    @TableField("LINE_NO")
    private String lineNo;

    /**
     * 线路全称
     */
    @TableField("FULL_LINE_NAME")
    private String fullLineName;

    /**
     * 收货人地址
     */
    @TableField("CUST_ADDRESS")
    private String custAddress;

    /**
     * 收货人地址吗
     */
    @TableField("CUST_ADDRESS_CODE")
    private String custAddressCode;

    /**
     * 收货人
     */
    @TableField("CONTACTOR_NAME")
    private String contactorName;

    /**
     * 收货人电话
     */
    @TableField("CUST_PHONE")
    private String custPhone;

    /**
     * 收货人邮箱
     */
    @TableField("CUST_MAIL")
    private String custMail;

    /**
     * 若接口转业务表数据有问题，调度时会对数据进行校验，当发现问题数据，给予相应的错误状态
     */
    @TableField("ERROR_STATUS")
    private String errorStatus;

    /**
     * 仓建标识,0:WMS自建，1：上游系统下传
     */
    @TableField("CREATE_FLAG")
    private String createFlag;

    /**
     * 回传标识
     */
    @TableField("RETURN_FLAG")
    private String returnFlag;

    /**
     * 出货备注
     */
    @TableField("EXP_REMARK")
    private String expRemark;

    /**
     * 附属单标识:0非附属;1附属
     */
    @TableField("BELONG_FLAG")
    private String belongFlag;

    /**
     * 上传标识
     */
    @TableField("SEND_FLAG")
    private String sendFlag;

    /**
     * 月台试算线路
     */
    @TableField("BUFFER_LINE_NO")
    private String bufferLineNo;

    /**
     * '是否是二类精神药品配送转自提，0：非精神药品；1：精神药品';
     */
    @TableField("SPECIAL_ARTICLE_GROUP")
    private String specialArticleGroup;

    /**
     * 00:初始状态；05已调度、10拣货完成、20内复核完成、30外复核完成、50装车完成
     */
    @TableField("EXP_STATUS")
    private String expStatus;

    /**
     * 存储类型：1：一般存储；2：客户；3：订单；4：供应商
     */
    @TableField("STOCK_TYPE")
    private String stockType;

    /**
     * 订货会期
     */
    @TableField("ORDER_PERIOD")
    private Integer orderPeriod;

    /**
     * 财务类型
     */
    @TableField("FINANCE_TYPE")
    private String financeType;

    /**
     * 是否被踢单标识，0：不是；1：是
     */
    @TableField("KICK_FLAG")
    private String kickFlag;

    /**
     * 特殊单的实际客户编码
     */
    @TableField("REAL_CUST_NO")
    private String realCustNo;

    /**
     * 特殊单的实际客户名称
     */
    @TableField("REAL_CUST_NAME")
    private String realCustName;

    /**
     * (特殊单)要货单位名称
     */
    @TableField("DEPT_NAME")
    private String deptName;

    /**
     * (特殊单)指定到货门店
     */
    @TableField("AGENT_NO")
    private String agentNo;

    /**
     * (特殊单)付款方式
     */
    @TableField("PAYMENT_TERM")
    private String paymentTerm;

    /**
     * 部门编号
     */
    @TableField("DEPT_NO")
    private String deptNo;

    /**
     * 客户订单号(平台订单号)
     */
    @TableField("CUST_EXP_NO")
    private String custExpNo;

    /**
     * ERP开票日期(又称出货单创建日期)
     */
    @TableField("ERPOPERATE_DATE")
    private LocalDateTime erpoperateDate;

    /**
     * 要求送货日期（界面的出货确认日期）预计出货日期
     */
    @TableField("CUSTSEND_DATE")
    private LocalDateTime custsendDate;

    /**
     * 0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("PRINT_FLAG")
    private String printFlag;

    /**
     * ERP保存时产生的订货单号
     */
    @TableField("ERP_NO")
    private String erpNo;

    /**
     * 差异原因
     */
    @TableField("DIFF_REASON")
    private String diffReason;

    /**
     * 建立人员
     */
    @TableField("RGST_NAME")
    private String rgstName;

    /**
     * 建立日期
     */
    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    /**
     * 更新人员
     */
    @TableField("UPDT_NAME")
    private String updtName;

    /**
     * 更新日期
     */
    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    /**
     * 波次号
     */
    @TableField("WAVE_NO")
    private String waveNo;

    /**
     * 承运商编号
     */
    @TableField("SHIPPER_NO")
    private String shipperNo;

    /**
     * 企业编号
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 快递单号
     */
    @TableField("SHIPPER_DELIVER_NO")
    private String shipperDeliverNo;

    /**
     * 配送站点
     */
    @TableField("DELIVER_ADDRESS")
    private String deliverAddress;

    /**
     * 是否打印发票。0：不打印；1：打印
     */
    @TableField("PRINT_BILL_FLAG")
    private String printBillFlag;

    /**
     * 订单来源。（1、京东；2、1号店；3、天猫；0，其他）
     */
    @TableField("ORDER_SOURCE")
    private String orderSource;

    /**
     * 机构，A0000L总仓
     */
    @TableField("ORG_NO")
    private String orgNo;

    /**
     * 字符预留字段，用于记录月台的区域的流水号
     */
    @TableField("RSV_VAROD1")
    private String rsvVarod1;

    /**
     * 字符预留字段，【铁越导入的出货单顺序】
     */
    @TableField("RSV_VAROD2")
    private String rsvVarod2;

    /**
     * 字符预留字段,【保税布控状态；0：不布控；1：布控】
     */
    @TableField("RSV_VAROD3")
    private String rsvVarod3;

    /**
     * 字符预留字段,【海关订单批次号】
     */
    @TableField("RSV_VAROD4")
    private String rsvVarod4;

    /**
     * 字符预留字段
     */
    @TableField("RSV_VAROD5")
    private String rsvVarod5;

    /**
     * 字符预留字段
     */
    @TableField("RSV_VAROD6")
    private String rsvVarod6;

    /**
     * 字符预留字段
     */
    @TableField("RSV_VAROD7")
    private String rsvVarod7;

    /**
     * 字符预留字段
     */
    @TableField("RSV_VAROD8")
    private String rsvVarod8;

    /**
     * 数值预留字段
     */
    @TableField("RSV_NUM1")
    private Double rsvNum1;

    /**
     * 数值预留字段
     */
    @TableField("RSV_NUM2")
    private Double rsvNum2;

    /**
     * 数值预留字段
     */
    @TableField("RSV_NUM3")
    private Double rsvNum3;

    /**
     * 日期预留字段
     */
    @TableField("RSV_DATE1")
    private LocalDateTime rsvDate1;

    /**
     * 日期预留字段
     */
    @TableField("RSV_DATE2")
    private LocalDateTime rsvDate2;

    /**
     * 日期预留字段
     */
    @TableField("RSV_DATE3")
    private LocalDateTime rsvDate3;

    /**
     * 提货类型：0-仓库提货；1-自提货
     */
    @TableField("TAKE_TYPE")
    private String takeType;

    /**
     * 最晚出货日期
     */
    @TableField("LAST_CUSTSEND_DATE")
    private LocalDateTime lastCustsendDate;

    /**
     * 报表上传流水号
     */
    @TableField("REPORT_UP_SERIAL")
    private Long reportUpSerial;

    /**
     * 店铺号
     */
    @TableField("SHOP_NO")
    private String shopNo;

    /**
     * 品项数
     */
    @TableField("SKUCOUNT")
    private Double skucount;

    /**
     * 发货人地址
     */
    @TableField("SEND_ADDRESS")
    private String sendAddress;

    /**
     * 发货人地址码
     */
    @TableField("SEND_ADDRESS_CODE")
    private String sendAddressCode;

    /**
     * 发货人
     */
    @TableField("SEND_NAME")
    private String sendName;

    /**
     * 发货人公司名称
     */
    @TableField("SEND_COMPANY_NAME")
    private String sendCompanyName;

    /**
     * 发货人邮编
     */
    @TableField("SEND_POSTCODE")
    private String sendPostcode;

    /**
     * 发货人手机
     */
    @TableField("SEND_MOBILE_PHONE")
    private String sendMobilePhone;

    /**
     * 发货人固定电话
     */
    @TableField("SEND_TELEPHONE")
    private String sendTelephone;

    /**
     * 发货国家二字码
     */
    @TableField("SEND_JPN")
    private String sendJpn;

    /**
     * 发货省份
     */
    @TableField("SEND_PROVINCE")
    private String sendProvince;

    /**
     * 发货市
     */
    @TableField("SEND_CITY")
    private String sendCity;

    /**
     * 发货区
     */
    @TableField("SEND_ZONE")
    private String sendZone;

    /**
     * 发货村镇
     */
    @TableField("SEND_COUNTRY")
    private String sendCountry;

    /**
     * 收货公司名称
     */
    @TableField("RECEIVE_COMPANY_NAME")
    private String receiveCompanyName;

    /**
     * 收货人固定电话
     */
    @TableField("RECEIVE_TELEPHONE")
    private String receiveTelephone;

    /**
     * 收货人国家二字码
     */
    @TableField("RECEIVE_JPN")
    private String receiveJpn;

    /**
     * 收货省份
     */
    @TableField("RECEIVE_PROVINCE")
    private String receiveProvince;

    /**
     * 收货市
     */
    @TableField("RECEIVE_CITY")
    private String receiveCity;

    /**
     * 收货区
     */
    @TableField("RECEIVE_ZONE")
    private String receiveZone;

    /**
     * 收货村镇
     */
    @TableField("RECEIVE_COUNTRY")
    private String receiveCountry;

    /**
     * 提货人地址
     */
    @TableField("TAKE_ADDRESS")
    private String takeAddress;

    /**
     * 提货人地址码
     */
    @TableField("TAKE_ADDRESS_CODE")
    private String takeAddressCode;

    /**
     * 提货人
     */
    @TableField("TAKE_NAME")
    private String takeName;

    /**
     * 提货人公司名称
     */
    @TableField("TAKE_COMPANY_NAME")
    private String takeCompanyName;

    /**
     * 提货人邮编
     */
    @TableField("TAKE_POSTCODE")
    private String takePostcode;

    /**
     * 提货人手机
     */
    @TableField("TAKE_MOBILE_PHONE")
    private String takeMobilePhone;

    /**
     * 提货人固定电话
     */
    @TableField("TAKE_TELEPHONE")
    private String takeTelephone;

    /**
     * 提货地国家二字码
     */
    @TableField("TAKE_JPN")
    private String takeJpn;

    /**
     * 提货省份
     */
    @TableField("TAKE_PROVINCE")
    private String takeProvince;

    /**
     * 提货市
     */
    @TableField("TAKE_CITY")
    private String takeCity;

    /**
     * 提货区
     */
    @TableField("TAKE_ZONE")
    private String takeZone;

    /**
     * 提货村镇
     */
    @TableField("TAKE_COUNTRY")
    private String takeCountry;

    /**
     * 送货日期类型：送货（日期）类型（1-只工作日送货(双休日、假日不用送);2-只双休日、假日送货(工作日不用送);3-工作日、双休日与假日均可送货;其他值-返回‘任意时间’）
     */
    @TableField("DELIVERDATE_TYPE")
    private String deliverdateType;

    /**
     * 发票打印：0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("ENVOICE_PRINT_STATUS")
    private String envoicePrintStatus;

    /**
     * 快递面单打印：0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("WAYBILL_PRINT_STATUS")
    private String waybillPrintStatus;

    /**
     * 内置装箱单打印：0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("PACKLIST_PRINT_STATUS")
    private String packlistPrintStatus;

    /**
     * 预留属性打印：0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("VAR1_PRINT_STATUS")
    private String var1PrintStatus;

    /**
     * 预留属性打印：0-未打印 1-打印1次 2-打印2次````以此类推
     */
    @TableField("VAR2_PRINT_STATUS")
    private String var2PrintStatus;

    /**
     * 订单类型：1：一单一品；2：一单多品
     */
    @TableField("SKU_COUNT_MODE")
    private String skuCountMode;

    @TableField("QRCODE")
    private Blob qrcode;


}
