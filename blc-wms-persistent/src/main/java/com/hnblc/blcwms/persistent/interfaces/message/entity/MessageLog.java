package com.hnblc.blcwms.persistent.interfaces.message.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 记录接口接收内容情况
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_M_MESSAGE_LOG")
public class MessageLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一主键
     */
    @TableId("SEQ_NO")
    private String seqNo;

    /**
     * 接收时间
     */
    @TableField("RECEIVE_DATE")
    private LocalDateTime receiveDate;

    /**
     * 来自客户端
     */
    @TableField("SEND_CLIENT_ID")
    private String sendClientId;

    /**
     * 报文备份文件
     */
    @TableField("MESSAGE_FILE")
    private String messageFile;

    /**
     * 处理结果状态码
     */
    @TableField("PROCESS_STATUS")
    private String processStatus;



}
