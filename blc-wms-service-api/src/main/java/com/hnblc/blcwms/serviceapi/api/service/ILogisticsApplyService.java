package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.serviceapi.api.dto.logistics.WaybillInfo;

import java.util.List;

public interface ILogisticsApplyService {
    /**
     * 批量获取运单信息，通用入口，包括判断申请目标地址
     * 单条获取也调用此接口
     * @param expressQueryVO 查询参数
     * @param logisticNo 要获取的物流商代码
     */
    Response<List<WaybillInfo>> applyWaybillInfo(ExpressQueryVO expressQueryVO,String logisticNo);

    /**
     * 批量获取拼多多专用圆通运单信息，
     * 单条获取也调用此接口
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<List<WaybillInfo>> applyPDDYTOWaybillInfo(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 获取圆通运单信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<List<WaybillInfo>> applyYTOWaybillInfo(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 批量获取申通运单信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<List<WaybillInfo>> applySTOWaybillInfo(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 获取圆通三段码信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<WaybillInfo> getYTOThreeSegCode(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 获取顺丰三段码信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<WaybillInfo> getSFThreeSegCode(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 获取申通三段码信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<WaybillInfo> getSTThreeSegCode(List<ExpressInfoHead> expressInfoHeads);

    /**
     * 获取韵达三段码信息
     * @param expressInfoHeads 从业务数据库中查询出的需要获取物流信息的单据集合
     */
    Response<WaybillInfo> getYDThreeSegCode(List<ExpressInfoHead> expressInfoHeads);
}
