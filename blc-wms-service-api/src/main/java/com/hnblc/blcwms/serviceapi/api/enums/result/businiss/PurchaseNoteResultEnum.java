package com.hnblc.blcwms.serviceapi.api.enums.result.businiss;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum PurchaseNoteResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","保存成功","SUCCESS"),
    ERROR_EMPTY_ARTICLE_ARRAY(false,"11","未读取到进货单商品详情数据","ERROR_EMPTY_ARTICLE_ARRAY"),
    ERROR_READ_FAIL(false,"12","订单内商品代码缺少基础信息：{0}","ERROR_READ_FAIL"),
    ERROR_SKU_COUNT(false,"13","订单商品种类数与收到详细信息不一致","ERROR_CREATE_ERROR_SKU_COUNT"),
    ERROR_UNKNOWN_SUPPLIER_NO(false,"14","供应商编码不存在","UNKNOWN_SUPPLIER_NO"),
    ERROR_UNKNOWN_SKU(false,"15","商品代码不存在，未创建基础资料","UNKNOWN_SKU"),
    ERROR_WRONG_PURCHASE_TYPE(false,"16","采购单入库类型错误","WRONG_PURCHASE_TYPE"),
    ERROR_SAVE_FAIL(false,"99","保存时发生未知错误：{0}","SAVE_FAIL"),

    CREATE_NOTE_ERROR_EXISTED(false,"18","该进货通知单已存在接口缓冲区，请等待处理","ORDER_EXIST");


    private String code;
    private String category="13";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    PurchaseNoteResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (PurchaseNoteResultEnum enums : EnumSet.allOf(PurchaseNoteResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
