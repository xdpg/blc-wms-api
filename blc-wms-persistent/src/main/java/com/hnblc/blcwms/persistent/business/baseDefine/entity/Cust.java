package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFCUST")
public class Cust implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 委托业主编码
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 委托业主客户编码
     */
    @TableField("OWNER_CUST_NO")
    private String ownerCustNo;

    /**
     * CUST_FLAG=1或2时，CUST_NO为大客户
     */
    @TableField("CUST_NO")
    private String custNo;

    /**
     * 0:委托业主客户；1：虚拟客户；2：实体客户；接口下传时默认为0，1、2是仓库内自建的
     */
    @TableField("CUST_FLAG")
    private String custFlag;

    /**
     * 0：自营；1：加盟；2：外仓（对应的外复核建单无需上传ERP审核）
     */
    @TableField("CUST_TYPE")
    private Integer custType;

    /**
     * 0:不支持拆零（如：批发客户）、1:部分拆零（整箱要货时不可以拆零，零散要货可以拆零）、2:可拆零（整件要货可以拆零）；
     */
    @TableField("SHIPPING_METHOD")
    private String shippingMethod;

    /**
     * 0-不用物流箱；1：用物流箱
     */
    @TableField("BOX_DELIVER")
    private String boxDeliver;

    /**
     * 客户名称
     */
    @TableField("CUST_NAME")
    private String custName;

    /**
     * 客户简称
     */
    @TableField("CUST_ALIAS")
    private String custAlias;

    /**
     * 客户地址
     */
    @TableField("CUST_ADDRESS")
    private String custAddress;

    /**
     * 客户邮编
     */
    @TableField("CUST_POSTCODE")
    private String custPostcode;

    /**
     * 送货地址
     */
    @TableField("DELIVERY_ADDRESS")
    private String deliveryAddress;

    /**
     * 客户电话1
     */
    @TableField("CUST_PHONE1")
    private String custPhone1;

    /**
     * 客户电话2
     */
    @TableField("CUST_PHONE2")
    private String custPhone2;

    /**
     * 客户传真1
     */
    @TableField("CUST_FAX1")
    private String custFax1;

    /**
     * 客户传真2
     */
    @TableField("CUST_FAX2")
    private String custFax2;

    /**
     * 客户EMAIL1
     */
    @TableField("CUST_EMAIL1")
    private String custEmail1;

    /**
     * 客户EMAIL2
     */
    @TableField("CUST_EMAIL2")
    private String custEmail2;

    /**
     * 联系人1
     */
    @TableField("CONTACTOR_NAME1")
    private String contactorName1;

    /**
     * 联系人2
     */
    @TableField("CONTACTOR_NAME2")
    private String contactorName2;

    /**
     * 门店标识
     */
    @TableField("INVOICE_NO")
    private String invoiceNo;

    /**
     * 发票号
     */
    @TableField("INVOICE_ADDR")
    private String invoiceAddr;

    /**
     * 发票地址
     */
    @TableField("INVOICE_HEADER")
    private String invoiceHeader;

    /**
     * 发票抬头
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 1-正常；0-不正常；
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 允许车辆最大吨位数
     */
    @TableField("MAX_CAR_TONNAGE")
    private Double maxCarTonnage;

    /**
     * 收货时段
     */
    @TableField("RECEIVING_HOURS")
    private String receivingHours;

    /**
     * 是否ERP下传   1：ERP下传单据；0：手工建单

     */
    @TableField("CREATE_FLAG")
    private String createFlag;

    /**
     * 部门
     */
    @TableField("CUST_DEPT")
    private String custDept;

    /**
     * 省份
     */
    @TableField("CUST_PROVINCE")
    private String custProvince;

    /**
     * 市
     */
    @TableField("CUST_CITY")
    private String custCity;

    /**
     * 区/县
     */
    @TableField("CUST_ZONE")
    private String custZone;

    /**
     * 客户注记码
     */
    @TableField("CUST_NOTECODE")
    private String custNotecode;

    /**
     * 外贸标识
     */
    @TableField("TRADE_FLAG")
    private String tradeFlag;

    /**
     * 客户是否要求单一到期日，0：没要求；1：要求
     */
    @TableField("ONLY_DATE_FLAG")
    private String onlyDateFlag;

    /**
     * 客户采集材积标识：0：不采集，1：采集
     */
    @TableField("COLLECT_FLAG")
    private String collectFlag;

    /**
     * 容器材质(零散为1开头，箱型为2开头，板型为3开头)，
11物流箱，12筐，13纸箱，14保温箱；21纸箱；31木板，32塑料板
     */
    @TableField("CONTAINER_MATERIAL")
    private String containerMaterial;

    /**
     * Y－标签有特殊标记，N－标签没有特殊标记；
过分拣时，一个滑道可能会对应两家门店，若为Y时，打印的标签上面会有特殊标记，用于区分
     */
    @TableField("WARN_FLAG")
    private String warnFlag;

    /**
     * 客户分播顺序
     */
    @TableField("DIVIDE_ORDER")
    private Double divideOrder;

    /**
     * 建立人员
     */
    @TableField("RGST_NAME")
    private String rgstName;

    /**
     * 建立日期
     */
    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    /**
     * 更新人员
     */
    @TableField("UPDT_NAME")
    private String updtName;

    /**
     * 更新日期
     */
    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    /**
     * 配量优先等级，数字越大优先级越高
     */
    @TableField("PRIO_LEVEL")
    private Integer prioLevel;

    /**
     * 优先等级类型   0：普通；1：优先
     */
    @TableField("PRIO_TYPE")
    private String prioType;

    @TableField("CONTROL_TYPE")
    private String controlType;

    @TableField("CONTROL_VALUE")
    private Double controlValue;

    /**
     * 企业编号
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("ROW_ID")
    private Long rowId;

    @TableField("RSV_VAR1")
    private String rsvVar1;

    @TableField("RSV_VAR2")
    private String rsvVar2;

    @TableField("RSV_VAR3")
    private String rsvVar3;

    @TableField("RSV_VAR4")
    private String rsvVar4;

    @TableField("RSV_VAR5")
    private String rsvVar5;

    @TableField("RSV_VAR6")
    private String rsvVar6;

    @TableField("RSV_VAR7")
    private String rsvVar7;

    @TableField("RSV_VAR8")
    private String rsvVar8;

    @TableField("RSV_NUM1")
    private Double rsvNum1;

    @TableField("RSV_NUM2")
    private Double rsvNum2;

    @TableField("RSV_NUM3")
    private Double rsvNum3;

    @TableField("RSV_DATE1")
    private LocalDateTime rsvDate1;

    @TableField("RSV_DATE2")
    private LocalDateTime rsvDate2;

    @TableField("RSV_DATE3")
    private LocalDateTime rsvDate3;


}
