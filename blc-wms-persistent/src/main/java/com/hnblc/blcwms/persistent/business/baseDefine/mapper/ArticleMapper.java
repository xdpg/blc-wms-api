package com.hnblc.blcwms.persistent.business.basedefine.mapper;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
