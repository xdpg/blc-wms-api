package com.hnblc.blcwms.serviceapi.api.group.custom;

import com.hnblc.blcwms.serviceapi.api.group.BaseGroup;

/**
 * @ClassName: CustomsGroup
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/8/28
 * @Version: V1.0
 */
public interface CustomsGroup extends BaseGroup {
}
