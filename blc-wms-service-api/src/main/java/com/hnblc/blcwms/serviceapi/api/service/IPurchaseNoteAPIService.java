package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseBody;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseHead;

import java.util.List;

public interface IPurchaseNoteAPIService {

    Response<List<PurchaseBody>> createPurchaseNote(PurchaseHead purchaseHead, ClientAuth clientAuth);
}
