package com.hnblc.blcwms.persistent.ecssent.entrence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.ecssent.entrence.entity.PreBillHead;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PreBillMapper extends BaseMapper<PreBillHead>{

        @Select("<script>select pbh.bill_no,pbh.entry_no,pbh.in_e_port,pbh.voyage_no,pbh.decl_date,pbh.appr_date " +
                "from pre_bill_head pbh " +
                "where true " +
                "<if test='billNoList!=null'>" +
                " and (" +
                " <foreach collection='billNoList' item='billNo' open='(' close=')' separator='and'>" +
                " pbh.bill_no = #{billNo} " +
                " </foreach> " +
                " or " +
                " <foreach collection='billNoList' item='billNo' open='(' close=')' separator='and'>" +
                " pbh.entry_id = #{billNo} " +
                "</foreach> " +
                "</if>" +
                "</script>")
        List<PreBillHead> getGoodImportDeclinfos( List<String> billNoList);

        @Select("select pbh.bill_no,pbh.entry_id,pbh.in_e_port,pbh.voyage_no,pbh.decl_date,pbh.appr_date_1 " +
                "from ecssent.pre_bill_head pbh " +
                "where bill_no = #{billNo} or entry_id = #{billNo}")
        PreBillHead getGoodImportDeclinfo(String billNo);
}
