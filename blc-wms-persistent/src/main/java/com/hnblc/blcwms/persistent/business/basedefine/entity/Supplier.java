package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFSUPPLIER")
public class Supplier implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("OWNER_NO")
    private String ownerNo;

    @TableField("SUPPLIER_NO")
    private String supplierNo;

    @TableField("REAL_SUPPLIER_NO")
    private String realSupplierNo;

    /**
     * 实体供应商名称
     */
    @TableField("REAL_SUPPLIER_NAME")
    private String realSupplierName;

    @TableField("SUPPLIER_NAME")
    private String supplierName;

    @TableField("SUPPLIER_ALIAS")
    private String supplierAlias;

    @TableField("SUPPLIER_ADDRESS1")
    private String supplierAddress1;

    @TableField("SUPPLIER_PHONE1")
    private String supplierPhone1;

    @TableField("SUPPLIER_FAX1")
    private String supplierFax1;

    @TableField("SUPPLIER1")
    private String supplier1;

    @TableField("SUPPLIER_ADDRESS2")
    private String supplierAddress2;

    @TableField("SUPPLIER_PHONE2")
    private String supplierPhone2;

    @TableField("SUPPLIER_FAX2")
    private String supplierFax2;

    @TableField("SUPPLIER2")
    private String supplier2;

    @TableField("SUPPLIER_REMARK")
    private String supplierRemark;

    @TableField("INVOICE_NO")
    private String invoiceNo;

    @TableField("INVOICE_ADDR")
    private String invoiceAddr;

    @TableField("INVOICE_HEADER")
    private String invoiceHeader;

    @TableField("STATUS")
    private String status;

    @TableField("UNLOAD_FLAG")
    private Integer unloadFlag;

    @TableField("AREA_NO")
    private String areaNo;

    @TableField("CREATE_FLAG")
    private String createFlag;

    /**
     * 供应商注记码
     */
    @TableField("SUPPLIER_NOTE_CODE")
    private String supplierNoteCode;

    @TableField("DEPT_NO")
    private String deptNo;

    @TableField("RGST_NAME")
    private String rgstName;

    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    @TableField("UPDT_NAME")
    private String updtName;

    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("ROW_ID")
    private Long rowId;

    /**
     * 【铁越用省】
     */
    @TableField("RSV_VAR1")
    private String rsvVar1;

    /**
     * 【铁越用市】
     */
    @TableField("RSV_VAR2")
    private String rsvVar2;

    /**
     * 【铁越用区】
     */
    @TableField("RSV_VAR3")
    private String rsvVar3;

    @TableField("RSV_VAR4")
    private String rsvVar4;

    @TableField("RSV_VAR5")
    private String rsvVar5;

    @TableField("RSV_VAR6")
    private String rsvVar6;

    @TableField("RSV_VAR7")
    private String rsvVar7;

    @TableField("RSV_VAR8")
    private String rsvVar8;

    @TableField("RSV_NUM1")
    private Double rsvNum1;

    @TableField("RSV_NUM2")
    private Double rsvNum2;

    @TableField("RSV_NUM3")
    private Double rsvNum3;

    @TableField("RSV_DATE1")
    private LocalDateTime rsvDate1;

    @TableField("RSV_DATE2")
    private LocalDateTime rsvDate2;

    @TableField("RSV_DATE3")
    private LocalDateTime rsvDate3;


}
