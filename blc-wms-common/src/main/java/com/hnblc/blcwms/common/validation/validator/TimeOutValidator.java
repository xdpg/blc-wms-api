package com.hnblc.blcwms.common.validation.validator;

import com.hnblc.blcwms.common.constant.CommonConst;
import com.hnblc.blcwms.common.utils.PatternUtils;
import com.hnblc.blcwms.common.validation.annotation.NotOverTime;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TimeOutValidator implements ConstraintValidator<NotOverTime, String> {

    long maxOverTime;
    @Override
    public void initialize(NotOverTime constraintAnnotation) {
        this.maxOverTime = constraintAnnotation.maxOverTime();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)){
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(CommonConst.getRespDetail("validation.request.commonField.notEmpty")).
            addConstraintViolation();
            return false;
        }
        if (!PatternUtils.isTimeStamp(value)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(CommonConst.getRespDetail("validation.request.commonField.isTimestamp")).addConstraintViolation();
            return false;
        }
        //判断如果是10位，则补全到13位
        if (value.length()==10){
            value = value+"000";
        }
        return true;
//        return WmsTimeUtils.timestampOverTime(Long.valueOf(value), this.maxOverTime);

    }
}
