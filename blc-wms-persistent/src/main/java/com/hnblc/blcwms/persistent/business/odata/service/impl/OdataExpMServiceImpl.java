package com.hnblc.blcwms.persistent.business.odata.service.impl;

import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.mapper.OdataExpMMapper;
import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpMService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-07-26
 */
@Service
public class OdataExpMServiceImpl extends ServiceImpl<OdataExpMMapper, OdataExpM> implements IOdataExpMService {

    @Override
    public List<TraceInfo> selectTraceInfo(String expNo) {
        return this.baseMapper.selectTraceInfo(expNo);
    }

    @Override
    public List<ExpressInfoHead> getExpressFullInfo(ExpressQueryVO queryVO) {
        return this.baseMapper.getExpressFullInfo(queryVO);
    }
}
