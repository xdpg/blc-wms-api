package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_ARTICLE")
public class WmsArticle implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 传单单据编号：传单通讯使用(建议使用自动增加的系列号)
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 货主商品编码
     */
    @TableField("OWNER_ARTICLE_NO")
    private String ownerArticleNo;

    /**
     * 类别编码：商品小类代码，只能是小类编码
     */
    @TableField("GROUP_NO")
    private String groupNo;

    /**
     * 供应商编码
     */
    @TableField("SUPPLIER_NO")
    private String supplierNo;

    /**
     * 商品条码(主条码)
     */
    @TableField("BARCODE")
    private String barcode;

    /**
     * 商品名称
     */
    @TableField("ARTICLE_NAME")
    private String articleName;

    /**
     * 商品英文名称
     */
    @TableField("ENAME")
    private String ename;

    /**
     * ABC分级（默认值C）
     */
    @TableField("ABCID")
    private String abcid;

    /**
     * 基本单位
     */
    @TableField("UNIT")
    private String unit;

    /**
     * 包装数量（及包装系数）
     */
    @TableField("PACKING_QTY")
    private Double packingQty;

    /**
     * 规格（如：1*1等）（最小规格）
     */
    @TableField("SPEC")
    private String spec;

    /**
     * 长(单位：cm)（单品，非外包装）
     */
    @TableField("LENGTH")
    private Double length;

    /**
     * 宽(单位：cm)（单品，非外包装）
     */
    @TableField("WIDTH")
    private Double width;

    /**
     * 高(单位：cm)（单品，非外包装）
     */
    @TableField("HEIGHT")
    private Double height;

    /**
     * 重量(单位：g)（单品，非外包装）
     */
    @TableField("WEIGTH")
    private Double weigth;

    /**
     * 保存天数(单位：天)
     */
    @TableField("EXPIRY_DAYS")
    private Double expiryDays;

    /**
     * 商品定价
     */
    @TableField("PRICE")
    private Double price;

    /**
     * 说明与备注
     */
    @TableField("NOTES")
    private String notes;

    /**
     * 企业编码：无需插入值，仅保留默认值即可
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 委托业主编码：无需插入值，仅保留默认值即可
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 接收时间
     */
    @TableField("HANDLETIME")
    private LocalDateTime handletime;

    /**
     * 助记码
     */
    @TableField("ARTICLE_IDENTIFIER")
    private String articleIdentifier;

    /**
     * 状态 0:禁用，1:启用
     */
    @TableField("STATUS")
    private String status;

    /**
     * 0:计量；1:计重
     */
    @TableField("MEASURE_MODE")
    private String measureMode;

    /**
     * 最小操作包装数量
     */
    @TableField("QMIN_OPERATE_PACKING")
    private Double qminOperatePacking;

    /**
     * 最小操作包装单位
     */
    @TableField("QMIN_OPERATE_UNIT")
    private String qminOperateUnit;

    /**
     * 包装单位
     */
    @TableField("PACKING_UNIT")
    private String packingUnit;

    /**
     * 包装规格
     */
    @TableField("PACKING_SPEC")
    private String packingSpec;


}
