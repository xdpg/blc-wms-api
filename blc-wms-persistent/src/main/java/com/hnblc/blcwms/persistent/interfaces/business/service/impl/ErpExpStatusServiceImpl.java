package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StatusReadyTask;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ErpExpStatus;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.ErpExpStatusMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IErpExpStatusService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-02
 */
@Service
public class ErpExpStatusServiceImpl extends ServiceImpl<ErpExpStatusMapper, ErpExpStatus> implements IErpExpStatusService {

    @Override
    public void insertHistory(List<ErpExpStatus> taskList) {
        this.baseMapper.insertHistory(taskList);
    }

    @Override
    public List<StatusReadyTask> listReadyTask(){
        return this.baseMapper.listReadyTask();
    }
}
