package com.hnblc.blcwms.serviceapi.api.enums.code;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum UntreadQualityEnum implements BaseEnums {



    GOOD("1","良品","GOOD"),
    BAD("A","不良品","BAD");


    private String code;
    private String textCn;
    private String testEn;

    UntreadQualityEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (UntreadQualityEnum enums : EnumSet.allOf(UntreadQualityEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
