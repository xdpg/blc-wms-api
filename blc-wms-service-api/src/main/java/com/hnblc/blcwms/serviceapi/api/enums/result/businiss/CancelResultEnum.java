package com.hnblc.blcwms.serviceapi.api.enums.result.businiss;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum CancelResultEnum implements BaseResultEnums {


    CANCEL_SUCCESS(true,"10","取消成功","CANCEL_SUCCESS"),
    CANCEL_ERROR_NOT_FOUND(false,"11","未找到要取消的订单","NOT_FOUND"),
    CANCEL_ERROR_WRONG_STATUS(false,"12","该出货单不是可自助取消状态","WRONG_STATUS"),
    CANCEL_ERROR_SEND(false,"13","该出货单已发货","SEND");

    private String code;
    private String category="22";
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    CancelResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (CancelResultEnum enums : EnumSet.allOf(CancelResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
