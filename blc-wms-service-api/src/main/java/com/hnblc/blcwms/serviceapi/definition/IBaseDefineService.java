package com.hnblc.blcwms.serviceapi.definition;


import com.hnblc.blcwms.persistent.business.basedefine.entity.*;

import java.util.Map;

public interface IBaseDefineService {

    /**
     * 获取仓别供应商配置表
     * @param warehouseNo
     * @param shipperNo
     * @return
     */
    Shipper getShipperCashe(String warehouseNo, String shipperNo);

    /**
     * 获取缓存客户信息
     * @param enterpriseNo 企业代码
     * @param ownerNo 货主代码
     * @param custNo 客户代码
     * @return
     */
    Cust getCustCashe(String enterpriseNo,String ownerNo, String custNo);

    /**
     * 获取缓存货主供应商信息
     * @param ownerNo
     * @param supplierNo
     * @return
     */
    Supplier getSupplierCashe(String ownerNo, String supplierNo);

    /**
     * 获取缓存货主信息
     * @param owner
     * @return
     */
    Owner getOwnerCache(String owner);

    /**
     * 获取缓存商品信息
     * @param ownerNo
     * @param ownerArticleNo
     * @return
     */
    Article getArticleCache(String ownerNo, String ownerArticleNo);

    /**
     * 获取单号
     * @param procedureMap
     */
    void getSheetNo(Map<String,Object> procedureMap);

    /**
     * 回写品项明细数据
     * @param procedureMap
     */
    void initUpdateSku(Map<String,Object> procedureMap);

    /**
     * 初始化出货单据状态跟踪
     * @param procedureMap
     */
    void initStatus(Map<String,Object> procedureMap);


}
