package com.hnblc.blcwms.persistent.business.basedefine.service;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Owner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
public interface IOwnerService extends IService<Owner> {

}
