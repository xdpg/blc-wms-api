package com.hnblc.blcwms.rest.dto.o2o;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.hnblc.blcwms.rest.dto.restInteraction.ReadableRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class O2ORequest<T> extends ReadableRequest<T> {

    @ApiModelProperty(name = "客户端本次请求会话编号",position = 5,example = "424b51ee165c4a588533847e2346d9fd")
    @NotBlank(message = "{validation.request.commonField.notEmpty}")
    private String sessionId;
}
