package com.hnblc.blcwms.common.exceptions;

public class FileSavePathNotConfigException extends RuntimeException {


    private static final long serialVersionUID = -1856112702278159119L;

    public FileSavePathNotConfigException(){
        super();
    }

    public FileSavePathNotConfigException(String message){
        super(message);
    }

    public FileSavePathNotConfigException(String message, Throwable cause){
        super(message, cause);
    }

    public FileSavePathNotConfigException(Throwable cause){
        super(cause);
    }
}
