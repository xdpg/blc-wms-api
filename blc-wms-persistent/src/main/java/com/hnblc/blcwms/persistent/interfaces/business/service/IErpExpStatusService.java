package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StatusReadyTask;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ErpExpStatus;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-11-02
 */
public interface IErpExpStatusService extends IService<ErpExpStatus> {

    void insertHistory(List<ErpExpStatus> taskList);

    List<StatusReadyTask> listReadyTask();
}
