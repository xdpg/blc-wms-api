package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ConvertCustSet;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.ConvertCustSetMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IConvertCustSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
@Service
public class ConvertCustSetServiceImpl extends ServiceImpl<ConvertCustSetMapper, ConvertCustSet> implements IConvertCustSetService {

}
