package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StatusReadyTask;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ErpExpStatus;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-11-02
 */
public interface ErpExpStatusMapper extends BaseMapper<ErpExpStatus> {

    void insertHistory(List<ErpExpStatus> taskList);


    List<StatusReadyTask> listReadyTask();
}
