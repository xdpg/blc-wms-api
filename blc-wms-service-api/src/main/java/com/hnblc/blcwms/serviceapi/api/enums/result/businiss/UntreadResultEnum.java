package com.hnblc.blcwms.serviceapi.api.enums.result.businiss;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum UntreadResultEnum implements BaseResultEnums {



    CREATE_SUCCESS(true,"10","保存成功","SUCCESS"),
    CREATE_ERROR_SHEET_NO_NOT_FOUND(false,"11","原订单号或运单号不存在。","SHEET_NO_NOT_FOUND"),
    CREATE_ERROR_WRONG_TYPE(false,"12","错误的退货类型","WRONG_TYPE"),
    CREATE_ERROR_PRODUCT_NOT_FOUND(false,"13","部分退货时，未填充退货商品信息。","PRODUCT_NOT_FOUND"),
    CREATE_ERROR_WRONG_QUALITY_TYPE(false,"14","未设置商品品质类型","WRONG_QUALITY_TYPE"),
    CREATE_ERROR_WRONG_PRODUCT(false,"15","退货商品不包含在原订单内商品中。","WRONG_PRODUCT"),
    CREATE_ERROR_PRODUCT_BEYOND_AMOUNT(false,"16","退货商品数量超过原订单商品数量","PRODUCT_BEYOND_AMOUNT"),
    CREATE_ERROR_RETURN_NOTE_EXIST(false,"17","该退货通知单已存在接口缓冲区，请等待处理","RETURN_NOTE_EXIST"),
    CREATE_ERROR_WRONG_RETURN_WAY(false,"18","原订单号和原运单号不可同时为空","WRONG_RETURN_WAY"),
    CREATE_ERROR_ARTICLE_NOT_FOUND(false,"19","退货商品ID：{0}基础资料不存在","ARTICLE_NOT_FOUND"),
    CREATE_ERROR_UNKNOWN(false,"99","保存时发生未知错误：{0}","UNKNOWN");



    private String code;
    private String category="24";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    UntreadResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (UntreadResultEnum enums : EnumSet.allOf(UntreadResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
