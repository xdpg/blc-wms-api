package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFARTICLE")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("OWNER_NO")
    private String ownerNo;

    @TableField("OWNER_ARTICLE_NO")
    private String ownerArticleNo;

    @TableId("ARTICLE_NO")
    private String articleNo;

    @TableField("ARTICLE_NAME")
    private String articleName;

    @TableField("ARTICLE_ENAME")
    private String articleEname;

    @TableField("ARTICLE_ONAME")
    private String articleOname;

    @TableField("ARTICLE_ALIAS")
    private String articleAlias;

    /**
     * 商品类别
     */
    @TableField("GROUP_NO")
    private String groupNo;

    @TableField("BATCH_ID")
    private String batchId;

    /**
     * 商品识别码
     */
    @TableField("ARTICLE_IDENTIFIER")
    private String articleIdentifier;

    /**
     * 基本单位
     */
    @TableField("UNIT")
    private String unit;

    /**
     * 最小操作包装数
     */
    @TableField("QMIN_OPERATE_PACKING")
    private Double qminOperatePacking;

    @TableField("EXPIRY_DAYS")
    private Integer expiryDays;

    @TableField("ALARMRATE")
    private Double alarmrate;

    @TableField("FREEZERATE")
    private Double freezerate;

    @TableField("ABC")
    private String abc;

    /**
     * 单位体积
     */
    @TableField("UNIT_VOLUMN")
    private Double unitVolumn;

    /**
     * 单位重量
     */
    @TableField("UNIT_WEIGHT")
    private Double unitWeight;

    /**
     * 累加材积
     */
    @TableField("CUMULATIVE_VOLUMN")
    private Double cumulativeVolumn;

    @TableField("A_OUT")
    private String aOut;

    /**
     * 0:不规则商品 1:规则商品
     */
    @TableField("RULE_FLAG")
    private String ruleFlag;

    @TableField("SPEC")
    private String spec;

    @TableField("SELL_PRICE")
    private Double sellPrice;

    @TableField("STATUS")
    private String status;

    @TableField("CREATE_FLAG")
    private String createFlag;

    /**
     * 0:实体商品；1:虚拟商品;2-包材商品
     */
    @TableField("VIRTUAL_FLAG")
    private String virtualFlag;

    @TableField("TURN_OVER_RULE")
    private String turnOverRule;

    /**
     * 验收超量，默认为0（不超量），大于0表示超量比率，-1（无限超量）
     */
    @TableField("CHECK_EXCESS")
    private Integer checkExcess;

    /**
     * 返配超量比率
     */
    @TableField("UM_CHECK_EXCESS")
    private Integer umCheckExcess;

    /**
     * 拣货超量比率，默认为0（不超量），大于0表示超量比率
     */
    @TableField("PICK_EXCESS")
    private Integer pickExcess;

    /**
     * 分播超量比率，默认为0（不超量），大于0表示超量比率
     */
    @TableField("DIVIDE_EXCESS")
    private Integer divideExcess;

    /**
     * 温控标识。0：无温度限制；1：有温度限制
     */
    @TableField("TEMPERATURE_FLAG")
    private String temperatureFlag;

    /**
     * 0:计量；1:计重；2:计价
     */
    @TableField("MEASURE_MODE")
    private String measureMode;

    /**
     * 0:不扫描；1:要扫描
     */
    @TableField("SCAN_FLAG")
    private String scanFlag;

    @TableField("CHECK_QTY_FLAG")
    private String checkQtyFlag;

    @TableField("CHECK_QTY_RATE")
    private Integer checkQtyRate;

    @TableField("CHECK_WEIGHT_FLAG")
    private String checkWeightFlag;

    @TableField("CHECK_WEIGHT_RATE")
    private Integer checkWeightRate;

    /**
     * 0:不质检；1：质检
     */
    @TableField("QC_FLAG")
    private String qcFlag;

    @TableField("QC_RATE")
    private Integer qcRate;

    /**
     * 特殊商品考虑生产日期混属性(由类别继承)：0：不可混，1：可混
     */
    @TableField("MIX_FLAG")
    private String mixFlag;

    /**
     * 0:单人验收，1:双人验收
     */
    @TableField("DOUBLE_CHECK")
    private String doubleCheck;

    /**
     * 是否配码商品，0：正常商品，1：配码商品
     */
    @TableField("CODE_TYPE")
    private String codeType;

    /**
     * 分箱类别标识
     */
    @TableField("DIVIDE_BOX")
    private String divideBox;

    /**
     * 拆零标识，1：可拆零（无条件拆零）；2：根据订单（结合订单配置和客户设置判断是否可拆零）拆零
     */
    @TableField("DELIVER_TYPE")
    private String deliverType;

    @TableField("DEPT_NO")
    private String deptNo;

    /**
     * 进货策略
     */
    @TableField("I_STRATEGY")
    private String iStrategy;

    /**
     * 出货策略
     */
    @TableField("O_STRATEGY")
    private String oStrategy;

    /**
     * 补货策略
     */
    @TableField("M_STRATEGY")
    private String mStrategy;

    /**
     * 返配策略
     */
    @TableField("RI_STRATEGY")
    private String riStrategy;

    /**
     * 退货策略
     */
    @TableField("RO_STRATEGY")
    private String roStrategy;

    /**
     * 判断策略
     */
    @TableField("FC_STRATEGY")
    private String fcStrategy;

    /**
     * 找拣货位策略
     */
    @TableField("RSV_STRATEGY1")
    private String rsvStrategy1;

    @TableField("RSV_STRATEGY2")
    private String rsvStrategy2;

    @TableField("RSV_STRATEGY3")
    private String rsvStrategy3;

    @TableField("RSV_STRATEGY4")
    private String rsvStrategy4;

    @TableField("RSV_STRATEGY5")
    private String rsvStrategy5;

    @TableField("RSV_STRATEGY6")
    private String rsvStrategy6;

    /**
     * 【铁越用匹号（空调）】；【天天惠用风格】
     */
    @TableField("RSV_ATTR1")
    private String rsvAttr1;

    /**
     * 天天惠款号，对于款号系统有逻辑处理，不能用作其他属性
     */
    @TableField("RSV_ATTR2")
    private String rsvAttr2;

    /**
     * 【铁越用类型（柜机/挂机）】；【天天惠用尺码】
     */
    @TableField("RSV_ATTR3")
    private String rsvAttr3;

    /**
     * 【铁越用机型（内机/外机）】；【天天惠用色码】
     */
    @TableField("RSV_ATTR4")
    private String rsvAttr4;

    @TableField("RSV_ATTR5")
    private String rsvAttr5;

    @TableField("RSV_ATTR6")
    private String rsvAttr6;

    @TableField("RSV_ATTR7")
    private String rsvAttr7;

    @TableField("RSV_ATTR8")
    private String rsvAttr8;

    @TableField("RSV_ATTR9")
    private String rsvAttr9;

    @TableField("RSV_ATTR10")
    private String rsvAttr10;

    @TableField("RSV_ATTR11")
    private String rsvAttr11;

    @TableField("RSV_ATTR12")
    private String rsvAttr12;

    @TableField("RSV_ATTR13")
    private String rsvAttr13;

    @TableField("RSV_ATTR14")
    private String rsvAttr14;

    @TableField("RSV_ATTR15")
    private String rsvAttr15;

    @TableField("RSV_ATTR16")
    private String rsvAttr16;

    @TableField("RSV_ATTR17")
    private String rsvAttr17;

    @TableField("RSV_ATTR18")
    private String rsvAttr18;

    @TableField("RSV_ATTR19")
    private String rsvAttr19;

    @TableField("RSV_ATTR20")
    private String rsvAttr20;

    @TableField("RGST_NAME")
    private String rgstName;

    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    @TableField("UPDT_NAME")
    private String updtName;

    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    @TableField("SUPPLIER_NO")
    private String supplierNo;

    @TableField("PRINT_FLAG")
    private String printFlag;

    /**
     * 批次管理类型：1-只管批次；2-只管生产日期；
3-管批次和生产日期；4-都不管
     */
    @TableField("LOT_TYPE")
    private String lotType;

    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 商品主条码
     */
    @TableField("BARCODE")
    private String barcode;

    /**
     * 拣货方式B、C
     */
    @TableField("OPERATE_TYPE")
    private String operateType;

    @TableField("ROW_ID")
    private Long rowId;

    /**
     * 【铁越用应收类型（1:重量、2:材积、3:大件/小件、4:货值计费）】
     */
    @TableField("RSV_CONTROL1")
    private String rsvControl1;

    /**
     * 【铁越用应付类型（1:重量、2:材积、3:大件/小件、4:货值计费）】
     */
    @TableField("RSV_CONTROL2")
    private String rsvControl2;

    /**
     * 【铁越用低温或常温的标识，0表示低温，1表示常温】
     */
    @TableField("RSV_CONTROL3")
    private String rsvControl3;

    @TableField("RSV_CONTROL4")
    private String rsvControl4;

    @TableField("RSV_CONTROL5")
    private String rsvControl5;

    /**
     * 最小操作包装单位
     */
    @TableField("QMIN_OPERATE_PACKING_UNIT")
    private String qminOperatePackingUnit;

    /**
     * 基本包装数量
     */
    @TableField("UNIT_PACKING")
    private Double unitPacking;

    /**
     * 长
     */
    @TableField("A_LENGTH")
    private Double aLength;

    /**
     * 宽
     */
    @TableField("A_WIDTH")
    private Double aWidth;

    /**
     * 高
     */
    @TableField("A_HEIGHT")
    private Double aHeight;

    /**
     * 箱拣货区最大量
     */
    @TableField("MAX_QTY_C")
    private Double maxQtyC;

    /**
     * 零散拣货区最大量
     */
    @TableField("MAX_QTY_B")
    private Double maxQtyB;

    /**
     * 板标准堆叠量
     */
    @TableField("QPALETTE")
    private Double qpalette;


}
