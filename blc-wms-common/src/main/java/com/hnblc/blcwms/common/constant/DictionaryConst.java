package com.hnblc.blcwms.common.constant;

public class DictionaryConst {
    /**
     * 接口字典表 语义 常量
     */
    /**
     *  接口操作出货单记录表
     */
    //操作状态
    public final static String EXP_LOG__STATUS__SUCCESS="OPERATE_SUCCESS";
    public final static String EXP_LOG__STATUS__FAIL="OPERATE_FAIL";
    //操作类型
    public final static String EXP_LOG__OPREATE_TYPE__CREATE="CREATE_EXP";
    public final static String EXP_LOG__OPERATE_TYPE__CANCEL="CANCEL_EXP";
    //取消订单失败原因
    public final static String EXP_LOG__ERROR_CODE__NOT_FOUND="NOT_FOUND";
    public final static String EXP_LOG__ERROR_CODE__WRONG_STATUS="WRONG_STATUS";
    public final static String EXP_LOG__ERROR_CODE__SEND="SEND";





}
