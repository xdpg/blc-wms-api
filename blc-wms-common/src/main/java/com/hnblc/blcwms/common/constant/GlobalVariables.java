package com.hnblc.blcwms.common.constant;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * GlobalVariables
 *
 * @author <A>cheng.yy@neusoft.com</A>
 *
 * @date Apr 15, 2015
 *
 * @Copyright: © 2001-2015 东软集团股份有限公司
 *
 */
public interface GlobalVariables {

    static final String URI_ACTION_EXT = ".action";
    static final String URI_API_EXT = ".do";
    static final String URI_AJAX_EXT = ".json";
    static final String URI_AJAX_FORM_EXT = ".form";

    // 目录符号
    static final String HTTP_SEPARATOR = "http://";
    static final String DIR_SEPARATOR = "/";
    static final String FILE_SEPARATOR = ".";
    static final String COLON_SEPAPATOR = ":";

    static final String PARAM_MARK = "?";
    static final String PARAM_SEPARATOR = "&";
    static final String PARAM_SIGN = "=";
    // 空串
    static final String STRING_BLANK = "";
    static final String STRING_SPACE = " ";
    static final String STRING_TRUE = "true";
    static final String STRING_FALSE = "false";

    // Mail
    static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    static final String MAIL_SMTP_TTLS_ENABLE = "mail.smtp.starttls.enable";
    static final String MAIL_SMTP_SSL_ENABLE = "mail.smtp.EnableSSL.enable";
    static final String MAIL_SMTP_SSL_FACTORY_CLASS = "mail.smtp.socketFactory.class";
    static final String MAIL_SMTP_SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    static final String MAIL_SMTP_HOST = "mail.smtp.host";
    static final String MAIL_SMTP_PORT = "mail.smtp.port";
    static final String MAIL_SEPAPATOR = "@";

    // 页面最小容量
    static final long RECORD_PAGE_SIZE_MIN = 5L;
    // 页面最大容量
    static final long RECORD_PAGE_SIZE_MAX = 100L;
    // 页码最小值
    static final long RECORD_PAGE_NO_MIN = 1L;

    // Log level :TRACE, DEBUG, INFO, WARN, ERROR
    static final String LOG_LEVEL_TRACE = "TRACE";
    static final String LOG_LEVEL_DEBUG = "DEBUG";
    static final String LOG_LEVEL_INFO = "INFO";
    static final String LOG_LEVEL_WARN = "WARN";
    static final String LOG_LEVEL_ERROR = "ERROR";

    // Log category
    static final String LOG_CATEGORY_CONSOLE = "[NEU-ECOMMERCE-CONSOLE-LOG]";
    static final String LOG_CATEGORY_SYSTEM = "[NEU-ECOMMERCE-SYSTEM-LOG]";
    static final String LOG_CATEGORY_ACCESS = "[NEU-ECOMMERCE-ACCESS-LOG]";
    static final String LOG_CATEGORY_CONTROLLER = "[NEU-ECOMMERCE-CONTROLLER-LOG]";
    static final String LOG_CATEGORY_SERVICE = "[NEU-ECOMMERCE-SERVICE-LOG]";

    // 日期格式
    static final String DATE_FORMAT_YMDHMS = "yyyy-MM-dd HH:mm:ss";
    static final String DATE_FORMAT_YMD = "yyyy-MM-dd";

    // ES index name
    static final String ES_INDEX_CATEGORIES = "repository_categories";
    static final String ES_INDEX_ATTRIBUTES = "repository_attributes";
    static final String ES_INDEX_BRANDS = "repository_brands";

    static final String ES_INDEX_GOODS = "repository_goods";
    static final String ES_INDEX_STORES = "repository_stores";

    static final String ES_HIT_CATEGORY_RANGE = "ES_HIT_CATEGORY_RANGE";
    static final String ES_HIT_CATEGORIES_2 = "ES_HIT_CATEGORIES_2";
    static final String ES_HIT_CATEGORIES_3 = "ES_HIT_CATEGORIES_3";
    static final String ES_HIT_CATEGORIES_3_LIST = "ES_HIT_CATEGORIES_3_LIST";

    static final String ES_DIS_CATEGORY_RANGE = "ES_DIS_CATEGORY_RANGE";
    static final String ES_DIS_CATEGORIES = "ES_DIS_CATEGORIES";
    static final String ES_DIS_OTHER_CATEGORIES = "ES_DIS_OTHER_CATEGORIES";

    static final String ES_HIT_BRANDS = "ES_HIT_BRANDS";
    static final String ES_DIS_BRANDS = "ES_DIS_BRANDS";

    static final String ES_DIS_PRICERANGE = "ES_DIS_PRICERANGE";
    static final String ES_HIT_ATTRIBUTES = "ES_HIT_ATTRIBUTES";
    static final String ES_DIS_ATTRIBUTES = "ES_DIS_ATTRIBUTES";

    static final String ES_HIT_GOODS = "ES_HIT_GOODS";
    static final String ES_DIS_GOODS = "ES_DIS_GOODS";

    static final String ES_HIT_STORES = "ES_HIT_STORES";
    static final String ES_DIS_STORES = "ES_DIS_STORES";

    static final String ES_DIS_SELECTED_FILTERS = "ES_DIS_SELECTED_FILTERS";

    static final String ES_SEARCH_TIME = "ES_SEARCH_TIME";

    static final String ES_SEARCH_ORDER_DESC = "desc";
    static final String ES_SEARCH_ORDER_ASC = "asc";

    static final String ES_SPLIT_SEPARATOR = ",";
    static final String ES_HIP_SPLIT_SEPARATOR = "-";
    static final String ES_PR_SPLIT_SEPARATOR = "~";

    // 散列划分大小
    static final int DEFAULT_STEP = 2;

    static final Set<String> RENDER_PARAMS = new HashSet<String>() {
        private static final long serialVersionUID = -1875210119949838027L;
        {
            this.add("_DATA_");
            this.add("_CDATA_KEYS_");
            this.add("_COMP_PATH_");
            this.add("_IS_FIRST_SCREEN_");
            this.add("_MAINSITE_");
            this.add("_SEO_");
            this.add("pageNo");
            this.add("pageSize");
        }
    };

}
