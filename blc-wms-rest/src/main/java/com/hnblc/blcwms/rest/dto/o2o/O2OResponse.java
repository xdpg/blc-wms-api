package com.hnblc.blcwms.rest.dto.o2o;

import com.hnblc.blcwms.common.exceptions.GeneralException;
import com.hnblc.blcwms.common.interaction.RestResponse;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class O2OResponse<T> extends RestResponse<T> {

    private String sessionId;
    public O2OResponse(boolean success) {
        super(success);
    }

    public O2OResponse(GeneralException e){
        super(e);
    }
}
