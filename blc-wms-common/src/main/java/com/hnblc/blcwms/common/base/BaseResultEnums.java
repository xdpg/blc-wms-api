package com.hnblc.blcwms.common.base;

public interface BaseResultEnums extends BaseEnums {
    String category();
    boolean isSuccess();
}
