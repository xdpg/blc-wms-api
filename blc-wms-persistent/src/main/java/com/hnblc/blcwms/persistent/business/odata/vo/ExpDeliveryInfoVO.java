package com.hnblc.blcwms.persistent.business.odata.vo;

import lombok.Data;

/**
 *
 * 左关联表头信息的表体详情bean
 */
@Data
public class ExpDeliveryInfoVO {

    private String enterpriseNo;
    private String warehouseNo;
    private String expNo;
    private String ownerNo;
    private String custNo;
    private String sourceexpNo;
    private String shipperDeliverNo;
    private String expType;
    private String expStatus;
    private String articleNo;
    private String ownerArticleNo;
    private String articleName;
    private Double rowId;
    private Double planQty;
    private Double packingQty;
}
