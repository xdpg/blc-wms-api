package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("CONVERT_CUST_SET")
public class ConvertCustSet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * WMS企业代码
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * WMS销售单位客户（门店、线上店铺）代码
     */
    @TableField("CUST_NO")
    private String custNo;

    /**
     * WMS货主客户代码
     */
    @TableField("OWNER_CUST_NO")
    private String ownerCustNo;

    /**
     * WMS货主子客户代码
     */
    @TableField("SUB_CUST_NO")
    private String subCustNo;

    /**
     * ERP销售单位（门店、线上店铺）代码
     */
    @TableField("ERP_CUST_NO")
    private String erpCustNo;


}
