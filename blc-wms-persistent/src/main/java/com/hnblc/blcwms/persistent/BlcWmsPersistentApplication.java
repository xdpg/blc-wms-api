package com.hnblc.blcwms.persistent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlcWmsPersistentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlcWmsPersistentApplication.class, args);
    }

}
