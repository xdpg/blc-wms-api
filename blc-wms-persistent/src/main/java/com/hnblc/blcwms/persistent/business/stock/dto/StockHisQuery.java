package com.hnblc.blcwms.persistent.business.stock.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class StockHisQuery {


    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
    private String storeCode;

    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
    private String endDate;

}
