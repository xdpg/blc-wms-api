package com.hnblc.blcwms.rest.task;

import com.hnblc.blcwms.serviceapi.api.service.IRationUpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @ClassName: RestSimpleTask
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/11/1
 * @Version: V1.0
 */
@Component
public class RestSimpleTask {

    @Autowired
    IRationUpService rationUpService;
    Logger logger = LoggerFactory.getLogger(RestSimpleTask.class);
    @Scheduled(fixedDelay = 120000)
    public void simpleTask(){
//
//      logger.info("********************************[[Simple schedule task start]]*******************************\n");
//      中文日志
        logger.info("********************************[[定时任务开始]]*******************************\n");
        rationUpService.sendRetionUp();
//      logger.info("*********************************[[Simple schedule task end]]********************************\n");
//      中文日志
        logger.info("********************************[[定时任务结束]]********************************\n");
    }
}
