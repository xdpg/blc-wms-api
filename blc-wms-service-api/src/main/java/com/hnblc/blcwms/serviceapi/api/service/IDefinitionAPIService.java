package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.definition.ArticleCategory;
import com.hnblc.blcwms.serviceapi.api.dto.definition.ArticleInfo;
import com.hnblc.blcwms.serviceapi.api.dto.definition.BaseDefinition;
import com.hnblc.blcwms.serviceapi.api.dto.definition.Company;

import java.util.List;

public interface IDefinitionAPIService {


    /**
     * 创建货主，同时创建销售单位，既是货主，也是销售单位
     * @param company
     * @return
     */
    Response createOwnerWithCustom(Company company);
    Response createOwner(Company company);
    Response createCustomForSell(Company company);
    Response createSupplier(Company company);
    Response<List<ArticleCategory>> createCategories(BaseDefinition<List<ArticleCategory>> articleCategories, ClientAuth clientAuth);
    Response<List<ArticleInfo>> createArticleInfos(BaseDefinition<List<ArticleInfo>> articleInfos, ClientAuth clientAuth);
}
