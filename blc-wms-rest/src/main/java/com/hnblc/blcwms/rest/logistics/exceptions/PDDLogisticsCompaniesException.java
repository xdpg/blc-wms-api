package com.hnblc.blcwms.rest.logistics.exceptions;

public class PDDLogisticsCompaniesException extends PDDException {


    private static final long serialVersionUID = 2257658824953794455L;

    public PDDLogisticsCompaniesException(){
        super();
    }

    public PDDLogisticsCompaniesException(String message){
        super(message);
    }

    public PDDLogisticsCompaniesException(String message, Throwable cause){
        super(message, cause);
    }

    public PDDLogisticsCompaniesException(Throwable cause){
        super(cause);
    }
}
