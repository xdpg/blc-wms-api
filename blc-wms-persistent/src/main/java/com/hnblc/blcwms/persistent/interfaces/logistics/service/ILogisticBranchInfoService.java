package com.hnblc.blcwms.persistent.interfaces.logistics.service;

import com.hnblc.blcwms.persistent.interfaces.logistics.entity.LogisticBranchInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用于请求快递公司运单号使用的 发货人发货网点信息（前期主要用于拼多多专用网点） 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
public interface ILogisticBranchInfoService extends IService<LogisticBranchInfo> {

}
