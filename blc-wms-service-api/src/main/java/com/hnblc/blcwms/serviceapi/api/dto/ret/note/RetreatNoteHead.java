package com.hnblc.blcwms.serviceapi.api.dto.ret.note;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>Description: return goods back to supplier manifests
 *
 * </p> <br/>
 *
 * @author linsong <br>
 * @version 1.0 2020.01.20 16:33 <br>
 */
@Data
public class RetreatNoteHead{

    private String retreatNo;
    private String retreatType;
    private String supplierNo;
    private Date checkDate;
    private String checker;
    private String remark;
    private List<RetreatNoteBody> retreatDetails;
}
