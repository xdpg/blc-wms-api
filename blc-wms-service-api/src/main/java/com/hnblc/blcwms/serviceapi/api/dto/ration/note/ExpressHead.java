package com.hnblc.blcwms.serviceapi.api.dto.ration.note;

import com.hnblc.blcwms.serviceapi.api.group.custom.express.Exp1210Group;
import com.hnblc.blcwms.serviceapi.api.group.custom.express.ExpB2CGroup;
import com.hnblc.blcwms.serviceapi.api.group.custom.express.ExpGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ExpressHead {

    /**
     * 发货通知单号,一般为订单号
     */
    @ApiModelProperty(name = "发货通知单号",required = true,example = "1234567890",position = 1)
    @Length(max = 60)
    @NotEmpty(groups = {ExpGroup.class})
    private String sourceExpNo;

    /**
     * 物流运单号
     */
    @ApiModelProperty(name = "物流运单号",required = true,example = "891365495261",position = 2)
    @Length(max = 30)
//    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
    private String logisticWaybillNo;
    /**
     * 仓储维护承运商编码
     */
    @ApiModelProperty(name = "承运商代码",required = true,example = "L0007",position = 3)
    @Length(max = 15)
    @NotEmpty(groups = {ExpB2CGroup.class})
    private String logisticNo;
    /**
     * 出货类型：OE：普通出货，ID：直通出货 ，B2C：电商B2C出货(默认)  JH：9610业务包裹  OHQ：账册结转出区    OYF：样品担保出区
     */
    @ApiModelProperty(name = "出货类型",required = true,example = "2",position = 4)
    @Length(max = 1)
    @NotEmpty(groups = {ExpGroup.class})
    private String expType;
    /**
     * 直邮业务提单号（直邮专用）
     */
    @ApiModelProperty(name = "直邮业务提单号",example = "15789384848",position = 5)
    @Length(max = 20)
    private String mailPoNo;

    /**
     * 进出口9610业务，邮件包，快件包进出口类型：0：进口，1：出口
     */
    @ApiModelProperty(name = "直邮业务进出口类型",example = "15789384848",position = 6)
    @Length(max = 20)
    private String mailIeType;

    /**
     * 订单来源电商平台
     */
    @ApiModelProperty(name = "电商平台代码",example = "15789384848",position = 7)
    @Length(max = 20)
    @NotEmpty(groups = {ExpB2CGroup.class})
    private String orderSource;

    /**
     * 海关布控标志：0：不布控，1：布控
     */
    @ApiModelProperty(name = "布控查验标志",example = "0",position = 8)
    @Length(max = 1)
    @NotEmpty(groups = {Exp1210Group.class})
    private String customsInspection;


    /**
     * 发货人
     */
    @ApiModelProperty(name = "发货人",example = "李XX",position = 9)
    @Length(max = 50)
    private String shipper;


    /**
     * 发货人公司名称
     */
    @ApiModelProperty(name = "发货人公司名称",example = "河南进口物资公共保税中心集团有限公司",position = 9)
    @Length(max = 100)
    private String shipperCompanyName;

    /**
     * 发货人省
     */
    @ApiModelProperty(name = "发货人省份",example = "河南省",position = 10)
    @Length(max = 50)
    private String shipperProvince;

    /**
     * 发货人市
     */
    @ApiModelProperty(name = "发货人市",example = "郑州市",position = 11)
    @Length(max = 50)
    private String shipperCity;
    /**
     * 发货人区/县
     */
    @ApiModelProperty(name = "发货人区县",example = "金水区",position = 12)
    @Length(max = 50)
    private String shipperZone;
    /**
     * 发货人村镇/街道
     */
    @ApiModelProperty(name = "发货人村镇/街道",example = "丰产路街道",position = 13)
    @Length(max = 50)
    private String shipperStreet;
    /**
     * 发货人地址
     */
    @ApiModelProperty(name = "发货人地址",example = "北京市朝阳区长安街XX号",position = 14)
    @Length(max = 100)
    private String shipperAddress;
    /**
     * 发货人邮编
     */
    @ApiModelProperty(name = "发货人邮编",example = "010001",position = 15)
    @Length(max = 20)
    private String shipperPostcode;
    /**
     * 发货人手机
     */
    @ApiModelProperty(name = "发货人手机",example = "135xxxxxxxx",position = 16)
    @Length(max = 20)
    private String shipperMobilePhone;


    /**
     * 发货人固定电话
     */
    @ApiModelProperty(name = "发货人固定电话",example = "0371-xxxxxxxx",position = 17)
    @Length(max = 20)
    private String shipperPhoneNo;


    /**
     * 收货人
     */
    @ApiModelProperty(name = "收货人",example = "王XX",position = 18)
    @Length(max = 150)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiver;


//    /**
//     * 收货人公司名称
//     */
//    @ApiModelProperty(name = "发货人公司名称",example = "河南进口物资公共保税中心集团有限公司",position = 19)
//    @Length(max = 100)
//    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
//    private String receiveCompanyName;

//    /**
//     * 收货人固定电话
//     */
//    @ApiModelProperty(name = "收货人固定电话",example = "0371-xxxxxxxx",position = 19)
//    @Length(max = 20)
//    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
//    private String receivePhoneNo;


    /**
     * 收货人省
     */
    @ApiModelProperty(name = "收货人省份",example = "河南省",position = 20)
    @Length(max = 50)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverProvince;

    /**
     * 收货人市
     */
    @ApiModelProperty(name = "收货人市",example = "郑州市",position = 21)
    @Length(max = 50)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverCity;
    /**
     * 收货人区/县
     */
    @ApiModelProperty(name = "收货人区县",example = "金水区",position = 22)
    @Length(max = 50)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverZone;
    /**
     * 收货人村镇/街道
     */
    @ApiModelProperty(name = "收货人村镇/街道",example = "丰产路街道",position = 23)
    @Length(max = 50)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverStreet;
    /**
     * 收货人地址
     */
    @ApiModelProperty(name = "收货人地址",example = "北京市朝阳区长安街XX号",position = 24)
    @Length(max = 200)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverAddress;
    /**
     * 收货人电邮地址
     */
    @ApiModelProperty(name = "收货人电邮地址",example = "xxxx@XXX.com",position = 25)
    @Length(max = 50)
    private String receiverEMail;
    /**
     * 收货人手机
     */
    @ApiModelProperty(name = "收货人电话",example = "135xxxxxxxx",position = 26)
    @Length(max = 20)
    @NotEmpty(groups = {Exp1210Group.class})
    private String receiverMobileNo;

    /**
     * 订单批次号
     */
    @ApiModelProperty(name = "订单批次号",example = "MQWE15135050",position = 27)
    private String orderBatch;

    /**
     * 提货类型：0：快递，1：自提
     */
    @ApiModelProperty(name = "提货类型：0：快递，1：自提",example = "0",position = 27)
    @Length(max = 1)
    @NotEmpty(groups = {Exp1210Group.class})
    private String takeType;

    /**
     * 操作类型：1，创建，2，更新
     */
    @ApiModelProperty(name = "操作类型：1，创建，2，更新",example = "1",position = 28)
    @Length(max = 1)
    @NotEmpty(groups = {ExpGroup.class})
    private String operate;

    /**
     * 内件品类数
     */
    @ApiModelProperty(name = "内件品类数",example = "2",position = 29)
    @NotNull(groups = {ExpGroup.class})
    private Double skuCount;

    /**
     * 内件总数量
     */
    @ApiModelProperty(name = "内件总数量",example = "18",position = 29)
    @NotNull(groups = {ExpGroup.class})
    private Double goodCount;
    /**
     *出货备注
     */
    @ApiModelProperty(name = "出货备注",example = "包好一点",position = 30)
    @Length(max = 100)
    private String expRemark;


    /**
     * 出货日期：本单据预定出货日期,没有日期时，默认为当天。
     */
    @ApiModelProperty(name = "出货日期",example = "20190717",position = 31)
    private String preDate;


    /**
     * 是否紧急单：一般单=’0’、紧急单=’1’默认值一般单（紧急出货单）
     */
    @ApiModelProperty(name = "是否紧急单",example = "0",position = 32)
    private String fastFlag;
    /**
     * 承运商短地址
     */
    @ApiModelProperty(name = "承运商短地址",example = "010-2111-0550",position = 33)
    private String deliverAddress;


    List<ExpressBody> orderDetails;



}
