package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("CONVERT_ORG_SET")
public class ConvertOrgSet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 企业编码
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * wms机构号
     */
    @TableField("ORG_NO")
    private String orgNo;

    /**
     * erp机构号
     */
    @TableField("ERP_ORG_NO")
    private String erpOrgNo;


}
