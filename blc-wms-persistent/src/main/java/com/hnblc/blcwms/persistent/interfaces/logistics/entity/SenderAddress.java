package com.hnblc.blcwms.persistent.interfaces.logistics.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 网点下可用的发货地址，可根据网点和企业号仓别号获取发货地址
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_L_SENDER_ADDRESS")
public class SenderAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网点代码
     */
    @TableField("BRANCH_CODE")
    private String branchCode;

    /**
     * 仓储企业代码
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 仓库号
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    /**
     * 发货省
     */
    @TableField("PROVINCE")
    private String province;

    /**
     * 发货市
     */
    @TableField("CITY")
    private String city;

    /**
     * 发货区
     */
    @TableField("DISTRICT")
    private String district;

    /**
     * 发货详细地址
     */
    @TableField("DETAIL")
    private String detail;


}
