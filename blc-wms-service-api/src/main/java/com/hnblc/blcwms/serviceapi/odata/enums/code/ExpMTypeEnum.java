package com.hnblc.blcwms.serviceapi.odata.enums.code;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ExpMTypeEnum implements BaseEnums {


    OE("0","直通单出货","OE"),
    ID("1","直通单出货","ID"),
    B2C("2","电商出货","B2C"),
    JH("3","集货出库","JH"),
    B2B("4","门店配送","B2B"),
    OHQ("5","账册结转出库","OHQ"),
    OYP("6","样品担保出库","OYP"),
    OS("7","紧急单出库","OS"),
    OCJ("8","国检送检出库","OCJ"),
    OBC("9","包材出库","OBC"),
    OJ("10","客户自提","OJ"),
    B2C1("11","电商预包出库","B2C1");


    private String code;
    private String textCn;
    private String testEn;

    ExpMTypeEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (ExpMTypeEnum enums : EnumSet.allOf(ExpMTypeEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
