package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Supplier;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.SupplierMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.ISupplierService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements ISupplierService {

}
