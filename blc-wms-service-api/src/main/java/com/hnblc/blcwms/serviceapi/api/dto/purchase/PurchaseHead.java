package com.hnblc.blcwms.serviceapi.api.dto.purchase;

import com.hnblc.blcwms.serviceapi.api.group.owner.purchase.PurchaseGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class PurchaseHead {

    @NotBlank(groups = {PurchaseGroup.class})
    @Length(max = 30,groups = {PurchaseGroup.class})
    private String purchaseNo;
    @NotBlank(groups = {PurchaseGroup.class})
    @Length(max = 1,groups = {PurchaseGroup.class})
    private String purchaseType;
    @NotBlank(groups = {PurchaseGroup.class})
    @Length(max = 10,groups = {PurchaseGroup.class})
    private String supplierNo;
    private LocalDateTime checkDate;
    private String checker;
    private LocalDateTime arriveDate;
    private LocalDateTime validDate;
    private String remark;
    @NotEmpty(groups = {PurchaseGroup.class})
    private List<PurchaseBody> purchaseDetails;
}
