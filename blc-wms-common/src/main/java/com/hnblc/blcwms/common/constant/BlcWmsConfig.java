package com.hnblc.blcwms.common.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class BlcWmsConfig {


    @Value("${sys.version}")
    private String sys_version;

    @Value("${sys.interface.data.log.path.win}")
    private String sys_interface_data_log_path_win;

    @Value("${sys.interface.data.log.path.unix}")
    private String sys_interface_data_log_path_unix;

}
