package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsOwner;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsOwnerMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsOwnerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
@Service
public class WmsOwnerServiceImpl extends ServiceImpl<WmsOwnerMapper, WmsOwner> implements IWmsOwnerService {

}
