package com.hnblc.blcwms.persistent.interfaces.custom.service.impl;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomLogisInfo;
import com.hnblc.blcwms.persistent.interfaces.custom.mapper.CustomLogisInfoMapper;
import com.hnblc.blcwms.persistent.interfaces.custom.service.ICustomLogisInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 保存客户请求承运商信息 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
@Service
public class CustomLogisInfoServiceImpl extends ServiceImpl<CustomLogisInfoMapper, CustomLogisInfo> implements ICustomLogisInfoService {

}
