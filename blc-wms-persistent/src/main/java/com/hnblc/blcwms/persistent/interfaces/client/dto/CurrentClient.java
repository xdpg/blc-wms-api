package com.hnblc.blcwms.persistent.interfaces.client.dto;

import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CurrentClient extends ClientInfo {
    private static final long serialVersionUID = -3806705019859090933L;

    private List<ClientAuth> clientAuthInfo;
}
