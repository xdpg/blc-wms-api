package com.hnblc.blcwms.rest.service;

import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;
import com.hnblc.blcwms.persistent.interfaces.message.entity.MessageLog;

import javax.servlet.http.HttpServletRequest;

public interface IMessageService {
    MessageLog process(ClientInfo clientInfo, HttpServletRequest request);
    boolean processed(MessageLog messageLog);
}
