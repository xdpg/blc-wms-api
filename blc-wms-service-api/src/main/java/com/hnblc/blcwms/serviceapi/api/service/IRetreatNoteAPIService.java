package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.ret.note.RetreatNoteBody;
import com.hnblc.blcwms.serviceapi.api.dto.ret.note.RetreatNoteHead;

import java.util.List;

/**
 * <p>Description: 退厂通知单接口</p> <br/>
 *
 * @author linsong <br>
 * @version 1.0 2020.01.20 16:24 <br>
 */
public interface IRetreatNoteAPIService {
    Response<List<RetreatNoteBody>> createRetNote(RetreatNoteHead retreatNoteHead, ClientAuth clientAuth);
}
