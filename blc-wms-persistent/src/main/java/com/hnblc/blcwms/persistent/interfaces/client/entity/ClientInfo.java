package com.hnblc.blcwms.persistent.interfaces.client.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 接口客户端信息表
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_C_CLIENT_INFO")
public class ClientInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客户端ID
     */
    @TableId("CLIENT_ID")
    private String clientId;

    /**
     * 客户端名称
     */
    @TableField("CLIENT_NAME")
    private String clientName;

    /**
     * 客户端接入验证码
     */
    @TableField("CLIENT_KEY")
    private String clientKey;

    /**
     * 是否启用
     */
    @TableField("ENABLED")
    private String enabled;

    /**
     * 是否加密
     */
    @TableField("ENCRYPT")
    private String encrypt;

    /**
     * 私钥
     */
    @TableField("PRIVATE_KEY")
    private String privateKey;

    /**
     * 公钥
     */
    @TableField("PUBLIC_KEY")
    private String publicKey;

    /**
     * 发货状态反馈地址
     */
    @TableField("CLIENT_RETURN_URL")
    private String clientReturnUrl;


}
