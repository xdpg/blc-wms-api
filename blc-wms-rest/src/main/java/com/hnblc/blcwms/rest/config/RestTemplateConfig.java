package com.hnblc.blcwms.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Description <br>
 * @ClassName RestTemplateConfig <br>
 * @Author linsong <br>
 * @date 2020.01.02 22:18 <br>
 */

@Configuration
public class RestTemplateConfig {


    /**
     * 处理请求头非application/json，但是需要解析json的请求
     * @return FullMediaTypeJackson2HttpMessageConverter 自定义json转换器
     */
//    @Bean
//    public FullMediaTypeJackson2HttpMessageConverter fullMediaTypeJackson2HttpMessageConverter(){
//        return new FullMediaTypeJackson2HttpMessageConverter();
//    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(fullMediaTypeJackson2HttpMessageConverter());
        return restTemplate;
    }
}
