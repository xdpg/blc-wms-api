package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hnblc.blcwms.common.enums.ResultEnum;
import com.hnblc.blcwms.common.interaction.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 接口操作出货单记录
 * </p>
 *
 * @author linsong
 * @since 2019-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_B_EXP_LOG")
public class ExpLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一键，由sequence提供
     */
    @TableId("SEQ_NO")
    private String seqNo;

    /**
     * 订单号，待取消的订单号，以及取消记录
     */
    @TableField("ORDER_NO")
    private String orderNo;

    /**
     * 操作客户端
     */
    @TableField("OPERATE_CLIENT")
    private String operateClient;

    /**
     * 操作类型,10:创建订单，16取消订单
     */
    @TableField("OPERATE_TYPE")
    private String operateType;

    /**
     * 操作成功失败
     */
    @TableField("STATUS")
    private String status;

    /**
     * 失败原因代码代码
     */
    @TableField("ERROR_CODE")
    private String errorCode;

    /**
     * 操作日期
     */
    @TableField("OPERATE_DATE")
    private LocalDateTime operateDate;

    /**
     * 原始报文ID
     */
    @TableField("MESSAGE_SEQ")
    private String messageSeq;

    public ExpLog(){}

    public ExpLog(String operateType, String operateClient, String messageSeq, String orderNo, Response result){
        this.operateType = operateType;
        this.operateClient = operateClient;
        this.messageSeq = messageSeq;
        this.orderNo =  orderNo;
        this.operateDate = LocalDateTime.now();
        this.errorCode = result.getDicCode();
        if (result.isSuccess()){
            this.status = ResultEnum.SUCCESS.code();
        }else {
            this.status = ResultEnum.FAILURE.code();
        }
    }


    public void buildError(String errorCode){
        this.errorCode = errorCode;
        this.status = "0";
    }
}
