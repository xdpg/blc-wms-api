package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Article;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.ArticleMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.IArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

}
