package com.hnblc.blcwms.serviceapi.api.dto.definition;


import com.hnblc.blcwms.serviceapi.api.group.owner.definition.DefinitionGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BaseDefinition<T> {

    @ApiModelProperty(name = "创建资料单号",position = 1)
    @NotBlank(groups = {DefinitionGroup.class})
    private String sheetId;
    @ApiModelProperty(name = "新建资料数据",position = 2)
    @NotNull(groups = {DefinitionGroup.class})
    @Valid
    private T definitionData;
}
