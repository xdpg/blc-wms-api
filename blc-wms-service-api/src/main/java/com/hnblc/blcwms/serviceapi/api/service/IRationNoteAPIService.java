package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.ration.cancel.ExpressCancel;
import com.hnblc.blcwms.serviceapi.api.dto.ration.note.ExpressHead;

import java.util.List;

public interface IRationNoteAPIService {


    /**
     * 取消订单，此方法需要访问业务库
     * @param clientAuth
     * @param orderCancelDtos
     * @param messageId
     * @return
     */
    List<ExpressCancel> cancelOrders(ClientAuth clientAuth, List<ExpressCancel> orderCancelDtos, String messageId);

    /**
     * 创建出货通知单
     * @param expressHead
     * @param clientAuth
     * @return
     */
    Response<Boolean> createRationNote(ExpressHead expressHead, ClientAuth clientAuth);
}
