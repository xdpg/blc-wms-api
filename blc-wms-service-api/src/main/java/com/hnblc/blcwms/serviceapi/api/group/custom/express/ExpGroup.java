package com.hnblc.blcwms.serviceapi.api.group.custom.express;

import com.hnblc.blcwms.serviceapi.api.group.custom.CustomsGroup;

/**
 * @ClassName: ExpGroup
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/8/28
 * @Version: V1.0
 */
public interface ExpGroup extends CustomsGroup {
}
