package com.hnblc.blcwms.serviceapi.api.dto.ret.note;

import lombok.Data;

/**
 * <p>Description: </p> <br/>
 *
 * @author linsong <br>
 * @version 1.0 2020.01.20 16:33 <br>
 */
@Data
public class RetreatNoteBody {
    private String ownerArticleNo;
    private String barcode;
    private String itemIndex;
    private String packingQty;
    private String planQty;
    private String errorDetail;
}
