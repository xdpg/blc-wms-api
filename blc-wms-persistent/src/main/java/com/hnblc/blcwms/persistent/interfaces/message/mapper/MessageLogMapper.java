package com.hnblc.blcwms.persistent.interfaces.message.mapper;

import com.hnblc.blcwms.persistent.interfaces.message.entity.MessageLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 记录接口接收内容情况 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface MessageLogMapper extends BaseMapper<MessageLog> {

}
