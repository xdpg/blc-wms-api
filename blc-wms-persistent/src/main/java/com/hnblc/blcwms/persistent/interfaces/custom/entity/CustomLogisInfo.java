package com.hnblc.blcwms.persistent.interfaces.custom.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 保存客户请求承运商信息
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_C_CUSTOM_LOGIS_INFO")
public class CustomLogisInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客户代码
     */
    @TableId("CUSTOM_NO")
    private String customNo;

    /**
     * 承运商代码
     */
    @TableField("LOGISTIC_NO")
    private String logisticNo;

    /**
     * 承运商接口字段1
     */
    @TableField("API_COLUMN1")
    private String apiColumn1;

    /**
     * 承运商接口字段2
     */
    @TableField("API_COLUMN2")
    private String apiColumn2;

    /**
     * 承运商接口字段3
     */
    @TableField("API_COLUMN3")
    private String apiColumn3;

    /**
     * 承运商接口字段4
     */
    @TableField("API_COLUMN4")
    private String apiColumn4;

    /**
     * 承运商接口字段5
     */
    @TableField("API_COLUMN5")
    private String apiColumn5;

    /**
     * 承运商接口字段6
     */
    @TableField("API_COLUMN6")
    private String apiColumn6;


}
