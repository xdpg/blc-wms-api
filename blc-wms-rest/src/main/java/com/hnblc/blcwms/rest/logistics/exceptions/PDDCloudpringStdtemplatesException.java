package com.hnblc.blcwms.rest.logistics.exceptions;


public class PDDCloudpringStdtemplatesException extends PDDException{


    private static final long serialVersionUID = -5753042736448627365L;

    public PDDCloudpringStdtemplatesException(){
        super();
    }

    public PDDCloudpringStdtemplatesException(String message){
        super(message);
    }

    public PDDCloudpringStdtemplatesException(String message, Throwable cause){
        super(message, cause);
    }

    public PDDCloudpringStdtemplatesException(Throwable cause){
        super(cause);
    }
}
