package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.hnblc.blcwms.persistent.interfaces.business.entity.DefinitionLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 接口操作基础资料记录 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-11-28
 */
public interface IDefinitionLogService extends IService<DefinitionLog> {

}
