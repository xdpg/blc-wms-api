package com.hnblc.blcwms.rest.config.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@ConfigurationProperties(prefix = "wms.custom.partner.pingduoduo")
@Data
@Component
public class STOProperties {

    @Value("${wms.custom.partner.sto.url}")
    private String url;
    @Value("${wms.custom.partner.sto.custom-name}")
    private String customName;
    @Value("${wms.custom.partner.sto.custom-password}")
    private String customPassword;
    @Value("${wms.custom.partner.sto.branch-name}")
    private String branchName;
    @Value("${wms.custom.partner.sto.signature}")
    private String signature;
    @Value("${wms.custom.partner.sto.batch-size}")
    private Integer batchSize;
}
