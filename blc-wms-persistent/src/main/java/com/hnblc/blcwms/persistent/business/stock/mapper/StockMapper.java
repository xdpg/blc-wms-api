package com.hnblc.blcwms.persistent.business.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.business.stock.dto.StockHisQuery;
import com.hnblc.blcwms.persistent.business.stock.entity.ArticleStock;
import com.hnblc.blcwms.persistent.business.stock.entity.HisStock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StockMapper extends BaseMapper<ArticleStock> {

    List<ArticleStock> querySellStock(List<StockHisQuery> stockHisQuery);

    List<HisStock> queryHisStock(StockHisQuery stockHisQuery);
}
