package com.hnblc.blcwms.persistent.interfaces.custom.service;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户在传单过程中是否有自定义收件人等信息 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
public interface ICustomInfoService extends IService<CustomInfo> {

}
