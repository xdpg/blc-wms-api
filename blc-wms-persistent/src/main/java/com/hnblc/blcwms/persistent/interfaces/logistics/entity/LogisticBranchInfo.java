package com.hnblc.blcwms.persistent.interfaces.logistics.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用于请求快递公司运单号使用的 发货人发货网点信息（前期主要用于拼多多专用网点）
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_L_LOGISTIC_BRANCH_INFO")
public class LogisticBranchInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网点代码
     */
    @TableId("BRANCH_CODE")
    private String branchCode;

    /**
     * 网点名称
     */
    @TableField("BRANCH_NAME")
    private String branchName;

    /**
     * 电子面单余量
     */
    @TableField("QUANTITY")
    private Long quantity;

    /**
     * 已回收用面单数量
     */
    @TableField("RECYCLED_QUANTITY")
    private Long recycledQuantity;

    /**
     * 取消的面单总数
     */
    @TableField("CANCEL_QUANTITY")
    private Long cancelQuantity;

    /**
     * 网点归属快递公司
     */
    @TableField("LOGISTIC_CODE")
    private String logisticCode;


}
