package com.hnblc.blcwms.common.base;

import com.hnblc.blcwms.common.constant.GlobalVariables;
import lombok.ToString;

import java.io.Serializable;

/**
 * 
 * BaseObject
 *
 * @author <A>cheng.yy@neusoft.com</A>
 *
 * @date Apr 15, 2015
 *
 * @Copyright: © 2001-2015 东软集团股份有限公司
 *
 */
@ToString
public abstract class BaseObject implements
        GlobalVariables, Serializable {

    private static final long serialVersionUID = -5288814752256367412L;
}
