package com.hnblc.blcwms.rest.logistics.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.serviceapi.api.dto.logistics.WaybillInfo;

import java.util.List;

/**
 * @Description <br>
 * @ClassName assdf <br>
 * @Author linsong <br>
 * @date 2020.01.02 18:18 <br>
 */
public interface ILogisticServiceTypeA {

    /**
     * 根据订单信息获取运单信息
     * @param expressInfoHeads 需要获取运单号的单据信息集合
     * @return Response<List<WaybillInfo>> 运单消息返回结果
     */
    Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads);
    /**
     * 根据订单信息获取运单信息
     * @param expressInfoHeads 需要获取运单号的单据信息集合
     * @return Response<List<WaybillInfo>> 运单消息返回结果
     */
    Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads, boolean isEncrypt);
    /**
     * 根据订单信息获取运单信息（电商平台专属单号专用）
     * @param expressInfoHeads 需要获取运单号的单据信息集合
     * @return Response<List<WaybillInfo>> 运单消息返回结果
     */
    Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads, boolean isEncrypt, String logisicCodeCn);

    /**
     * 取消已取用的单号
     * @param logisticCode 物流商代码
     * @param wayBillNo 运单号
     * @return Response<Boolean> 运单消息返回结果
     */
    Response<Boolean> cancelWaybillNo(String logisticCode, String wayBillNo);
}
