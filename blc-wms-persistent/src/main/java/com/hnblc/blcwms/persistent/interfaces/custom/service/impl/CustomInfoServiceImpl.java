package com.hnblc.blcwms.persistent.interfaces.custom.service.impl;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomInfo;
import com.hnblc.blcwms.persistent.interfaces.custom.mapper.CustomInfoMapper;
import com.hnblc.blcwms.persistent.interfaces.custom.service.ICustomInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户在传单过程中是否有自定义收件人等信息 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
@Service
public class CustomInfoServiceImpl extends ServiceImpl<CustomInfoMapper, CustomInfo> implements ICustomInfoService {

}
