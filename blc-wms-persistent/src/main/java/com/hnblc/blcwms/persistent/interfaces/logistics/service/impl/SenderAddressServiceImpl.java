package com.hnblc.blcwms.persistent.interfaces.logistics.service.impl;

import com.hnblc.blcwms.persistent.interfaces.logistics.entity.SenderAddress;
import com.hnblc.blcwms.persistent.interfaces.logistics.mapper.SenderAddressMapper;
import com.hnblc.blcwms.persistent.interfaces.logistics.service.ISenderAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 网点下可用的发货地址，可根据网点和企业号仓别号获取发货地址 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
@Service
public class SenderAddressServiceImpl extends ServiceImpl<SenderAddressMapper, SenderAddress> implements ISenderAddressService {

    @Override
    public SenderAddress getOneByLogisticCode(String logisticCode, String enterpriseNo, String warehouseNo) {
        return this.baseMapper.getOneByLogisticCode(logisticCode,enterpriseNo,warehouseNo);
    }
}
