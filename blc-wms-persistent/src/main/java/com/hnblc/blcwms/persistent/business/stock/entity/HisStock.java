package com.hnblc.blcwms.persistent.business.stock.entity;

import lombok.Data;

@Data
//@TableName("hisStock")
public class HisStock {
    private String itemNo;
    private String goodsNo;
    private String goodsName;
    private String unit;
    private String remainQty;
    private String storeInfo;
}
