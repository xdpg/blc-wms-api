package com.hnblc.blcwms.rest.dto.restInteraction;

import com.hnblc.blcwms.serviceapi.api.group.BaseGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class EncryptRequest extends BaseRequest {


    @ApiModelProperty(name = "身份签名信息",example = "424b51ee165c4a588533847e2346d9fd",position = 3)
    @NotBlank(message = "{validation.request.commonField.notEmpty}",groups = {BaseGroup.class})
    private String signature;
    @ApiModelProperty(name = "BASE64编码的业务数据",example = "424b51ee165c4a588533847e2346d9fd",position = 4)
    @NotBlank(message = "{validation.request.commonField.notEmpty}")
    private String businessData;
}
