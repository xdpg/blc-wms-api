package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRetration;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsRetrationMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsRetrationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-14
 */
@Service
public class WmsRetrationServiceImpl extends ServiceImpl<WmsRetrationMapper, WmsRetration> implements IWmsRetrationService {

}
