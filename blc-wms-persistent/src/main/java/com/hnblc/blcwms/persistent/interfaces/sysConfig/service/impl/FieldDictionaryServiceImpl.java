package com.hnblc.blcwms.persistent.interfaces.sysConfig.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.common.constant.CommonConst;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.entity.FieldDictionary;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.mapper.FieldDictionaryMapper;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.service.IFieldDictionaryService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 接口系统字段字典表 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-02-13
 */
@Service
public class FieldDictionaryServiceImpl extends ServiceImpl<FieldDictionaryMapper, FieldDictionary> implements IFieldDictionaryService {
    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_SYSTEM_DICTIONARIES_LIST,key = "#tableName.concat('::').concat(#tableColumn)")
    public List<FieldDictionary> listLoadInCache(String tableName, String tableColumn){
        QueryWrapper<FieldDictionary> wrapper = Wrappers.query();
        Map<String,String> paramMap = new HashMap();
        paramMap.put("ISENABLED","1");
        paramMap.put("TABLE_NAME",tableName);
        paramMap.put("TABLE_COLUMN",tableColumn);
        return this.list(wrapper.allEq(paramMap));
    }

    @Override
    @Cacheable(value = CommonConst.CACHE_KEY_SYSTEM_DICTIONARIES_MAP,key = "#tableName.concat('::').concat(#tableColumn)")
    public Map<String,FieldDictionary> mapLoadInCache(String tableName, String tableColumn){
        QueryWrapper<FieldDictionary> wrapper = Wrappers.query();
        Map<String,String> paramMap = new HashMap();
        paramMap.put("ISENABLED","1");
        paramMap.put("TABLE_NAME",tableName);
        paramMap.put("TABLE_COLUMN",tableColumn);

        Map<String,FieldDictionary> result = new HashMap<>();
        this.list(wrapper.allEq(paramMap)).forEach(item -> result.put(item.getColumnText(),item));
        return result;
    }
}
