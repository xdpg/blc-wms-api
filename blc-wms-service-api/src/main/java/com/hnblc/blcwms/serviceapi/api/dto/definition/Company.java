package com.hnblc.blcwms.serviceapi.api.dto.definition;

import com.hnblc.blcwms.serviceapi.api.group.owner.definition.DefinitionGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class Company {
    @NotBlank(groups = {DefinitionGroup.class})
    @Length(max = 20)
    private String sheetId;
    @NotBlank(groups = {DefinitionGroup.class})
    @Length(max = 15)
    private String enterpriseNo;
    @Length(max = 10)
    private String ownerNo;
    @NotBlank(groups = {DefinitionGroup.class})
    private String companyType;
    @NotBlank(groups = {DefinitionGroup.class})
    @Length(max = 10)
    private String companyNo;
    @NotBlank(groups = {DefinitionGroup.class})
    @Length(max = 90)
    private String companyName;
    @NotBlank(groups = {DefinitionGroup.class})
    @Length(max = 90)
    private String companyAlias;
    @Length(max = 20)
    private String companyPhone;
    @Length(max = 15)
    private String companyFax;
    @Length(max = 50)
    private String companyEmail;
    @Length(max = 120)
    private String companyAddress;
    @Length(max = 100)
    private String departmentNo;
    @Length(max = 100)
    private String province;
    @Length(max = 100)
    private String city;
    @Length(max = 100)
    private String zone;
    @Length(max = 20)
    private String companyContact;
    @Length(max = 20)
    private String companyRemark;
    @Length(max = 20)
    private String invoiceNo;
    @Length(max = 100)
    private String invoiceAddress;
    @Length(max = 50)
    private String invoiceHeader;

}
