package com.hnblc.blcwms.rest.logistics.exceptions;


public class PDDException extends RuntimeException{


    private static final long serialVersionUID = -4543668266904456962L;

    public PDDException(){
        super();
    }

    public PDDException(String message){
        super(message);
    }

    public PDDException(String message, Throwable cause){
        super(message, cause);
    }

    public PDDException(Throwable cause){
        super(cause);
    }
}
