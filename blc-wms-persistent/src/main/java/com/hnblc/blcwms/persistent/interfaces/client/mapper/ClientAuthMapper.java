package com.hnblc.blcwms.persistent.interfaces.client.mapper;

import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用于查询客户端有哪些客户的查询权限 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface ClientAuthMapper extends BaseMapper<ClientAuth> {

}
