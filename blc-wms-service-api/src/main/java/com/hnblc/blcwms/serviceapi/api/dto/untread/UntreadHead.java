package com.hnblc.blcwms.serviceapi.api.dto.untread;

import com.hnblc.blcwms.serviceapi.api.group.custom.express.UntreadGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @ClassName: UntreadHead
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/11/11
 * @Version: V1.0
 */
@Data
public class UntreadHead {
    /**
     * 物流运单号
     */
    @ApiModelProperty(name = "退货单号",required = true,example = "891365495261",position = 2)
    @Length(max = 20)
    @NotBlank(groups = {UntreadGroup.class})
    private String returnErpNo;
    @Length(max = 20)
    private String poOrderNo;
    @Length(max = 20)
    private String poLogisticNo;
    @NotBlank(groups = {UntreadGroup.class})
    private String poType;
    private String returnDate;
    private String entireFlag;
    private String qualityType;
    private String remark;
    @Valid
    private List<UntreadBody> untreadBodyList;
}
