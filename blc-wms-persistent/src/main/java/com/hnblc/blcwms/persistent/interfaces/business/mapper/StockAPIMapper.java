package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.hnblc.blcwms.persistent.interfaces.business.dto.StockParam;
import com.hnblc.blcwms.persistent.interfaces.business.entity.SalableStock;

import java.util.List;

public interface StockAPIMapper {
    List<SalableStock> getSalableStocks(StockParam stockParam);
}
