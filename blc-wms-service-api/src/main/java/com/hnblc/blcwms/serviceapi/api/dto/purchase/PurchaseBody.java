package com.hnblc.blcwms.serviceapi.api.dto.purchase;

import com.hnblc.blcwms.serviceapi.api.group.owner.purchase.PurchaseGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PurchaseBody {

    @NotBlank(groups = {PurchaseGroup.class})
    @Length(max = 30,groups = {PurchaseGroup.class})
    private String ownerArticleNo;
    @Length(max = 50,groups = {PurchaseGroup.class})
    private String barcode;
    @NotNull(groups = {PurchaseGroup.class})
    private Double itemIndex;
    @NotNull(groups = {PurchaseGroup.class})
    private Double packageQty;
    @NotNull(groups = {PurchaseGroup.class})
    private Double planQty;
    private String errorReason;
}
