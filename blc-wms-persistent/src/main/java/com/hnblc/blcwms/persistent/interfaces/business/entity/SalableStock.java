package com.hnblc.blcwms.persistent.interfaces.business.entity;


import lombok.Data;

@Data
public class SalableStock {
    private String articleIdentifier;
    private String ownerArticleNo;
    private String goodsName;
    private String barcode;
    private String unit;
    private String frozenQty;
    private String salableQty;
    private String storeInfo;
}
