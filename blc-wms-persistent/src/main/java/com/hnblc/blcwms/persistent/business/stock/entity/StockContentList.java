package com.hnblc.blcwms.persistent.business.stock.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("STOCK_CONTENT_LIST")
public class StockContentList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    @TableId("ROW_ID")
    private Double rowId;

    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 部门编码
     */
    @TableField("DEPT_NO")
    private String deptNo;

    @TableField("ARTICLE_NO")
    private String articleNo;

    @TableField("BARCODE")
    private String barcode;

    /**
     * 验收批次
     */
    @TableField("IMPORT_BATCH_NO")
    private String importBatchNo;

    @TableField("PRODUCE_DATE")
    private LocalDateTime produceDate;

    @TableField("EXPIRE_DATE")
    private LocalDateTime expireDate;

    @TableField("QUALITY")
    private String quality;

    @TableField("LOT_NO")
    private String lotNo;

    @TableField("RSV_BATCH1")
    private String rsvBatch1;

    @TableField("RSV_BATCH2")
    private String rsvBatch2;

    @TableField("RSV_BATCH3")
    private String rsvBatch3;

    @TableField("RSV_BATCH4")
    private String rsvBatch4;

    @TableField("RSV_BATCH5")
    private String rsvBatch5;

    @TableField("RSV_BATCH6")
    private String rsvBatch6;

    @TableField("RSV_BATCH7")
    private String rsvBatch7;

    @TableField("RSV_BATCH8")
    private String rsvBatch8;

    /**
     * 存储类型：1：一般存储；2：客户；3：订单；4：供应商
     */
    @TableField("STOCK_TYPE")
    private String stockType;

    /**
     * 对应的存储值：若一般存储为N，若SCAN_LABLE_NO=2，则写客户编号，若SCAN_LABLE_NO=3，写订单号；若SCAN_LABLE_NO=4，写供应商编码
     */
    @TableField("STOCK_VALUE")
    private String stockValue;

    @TableField("PACKING_QTY")
    private Double packingQty;

    /**
     * 实时库存
     */
    @TableField("QTY")
    private Double qty;

    /**
     * 发生数量
     */
    @TableField("MOVE_QTY")
    private Double moveQty;

    /**
     * 变动方向；1：增加；2：减少
     */
    @TableField("MOVE_TYPE")
    private String moveType;

    /**
     * 业务类型；I:一般进货；O:出货；RI：返配；RO:退货；MO:库存调账；FC:盘点
     */
    @TableField("PAPER_TYPE")
    private String paperType;

    /**
     * 业务单据号
     */
    @TableField("PAPER_NO")
    private String paperNo;

    /**
     * 记账日期；格式为：年月日
     */
    @TableField("PAPER_DATE")
    private LocalDateTime paperDate;

    @TableField("RGST_NAME")
    private String rgstName;

    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;


}
