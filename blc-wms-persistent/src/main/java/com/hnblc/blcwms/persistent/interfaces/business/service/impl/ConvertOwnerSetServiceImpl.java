package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ConvertOwnerSet;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.ConvertOwnerSetMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IConvertOwnerSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
@Service
public class ConvertOwnerSetServiceImpl extends ServiceImpl<ConvertOwnerSetMapper, ConvertOwnerSet> implements IConvertOwnerSetService {

}
