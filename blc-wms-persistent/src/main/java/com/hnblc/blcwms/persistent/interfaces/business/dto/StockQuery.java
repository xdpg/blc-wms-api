package com.hnblc.blcwms.persistent.interfaces.business.dto;

import lombok.Data;

/**
 * @ClassName: StockQuery
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/8/28
 * @Version: V1.0
 */

@Data
public class StockQuery {
    private String ownerArticleNo;
}
