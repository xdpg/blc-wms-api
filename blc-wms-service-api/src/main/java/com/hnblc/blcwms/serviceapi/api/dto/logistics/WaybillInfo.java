package com.hnblc.blcwms.serviceapi.api.dto.logistics;

import lombok.Data;

@Data
public class WaybillInfo {

    //拼多多专用属性
    private String objectId;
    private String orderNo;
    private String parentWaybillNo;
    private String waybillNo;
    private String deliveryAddress;
    private String bigShotCode;
    private String bigShotName;
    private String endBranchCode;
    private String endBranchName;
    private String printData;

}
