package com.hnblc.blcwms.persistent.interfaces.logistics.service;

import com.hnblc.blcwms.persistent.interfaces.logistics.entity.SenderAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 网点下可用的发货地址，可根据网点和企业号仓别号获取发货地址 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
public interface ISenderAddressService extends IService<SenderAddress> {


    /**
     * 通过物流商代码获取网点地址
     * @param logisticCode 对应物流商代码
     * @return
     */
    SenderAddress getOneByLogisticCode(String logisticCode,String enterpriseNo,String warehouseNo);

}
