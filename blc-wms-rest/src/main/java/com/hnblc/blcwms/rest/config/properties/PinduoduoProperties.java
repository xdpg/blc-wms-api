package com.hnblc.blcwms.rest.config.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@ConfigurationProperties(prefix = "wms.custom.partner.pingduoduo")
@Data
@Component
public class PinduoduoProperties {

    @Value("${wms.custom.partner.pingduoduo.auth-url}")
    private String authUrl;
    @Value("${wms.custom.partner.pingduoduo.url}")
    private String url;
    @Value("${wms.custom.partner.pingduoduo.client-id}")
    private String clientId;
    @Value("${wms.custom.partner.pingduoduo.client-secret}")
    private String clientSecret;
    @Value("${wms.custom.partner.pingduoduo.batch-size}")
    private Integer batchSize;
}
