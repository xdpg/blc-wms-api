package com.hnblc.blcwms.common.base;

public interface BaseEnums {

    String code();
    String getMessage();
}
