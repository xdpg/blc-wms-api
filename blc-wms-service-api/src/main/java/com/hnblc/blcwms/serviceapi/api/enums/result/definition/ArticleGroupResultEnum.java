package com.hnblc.blcwms.serviceapi.api.enums.result.definition;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ArticleGroupResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","保存或更新成功。","SUCCESS"),
    EMPTY_ARRAY(false,"11","未读取到类别数据。","EMPTY_ARRAY"),
    READ_FAIL(false,"12","未解析到类别数据。","READ_FAIL"),
    REPEAT_CATEGORY(false,"13","单次请求包含重复类别代码","REPEAT_CATEGORY"),
    SAVE_FAIL(false,"99","类别保存失败","SAVE_FAIL");



    private boolean success;
    private String code;
    private String category="13";
    private String textCn;
    private String testEn;

    ArticleGroupResultEnum(boolean success, String code, String textCn, String testEn){
        this.success = success;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess() {
        return this.success;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (ArticleGroupResultEnum enums : EnumSet.allOf(ArticleGroupResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
