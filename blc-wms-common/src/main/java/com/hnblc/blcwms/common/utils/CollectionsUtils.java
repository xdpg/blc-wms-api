package com.hnblc.blcwms.common.utils;

import java.util.*;

/**
 * @author linsong
 * list操作工具类
 */
public class CollectionsUtils {


    /**
     * 拆分List为多个List
     * @param list 被拆分的List
     * @param offset 偏移量，既每个目标List大小
     * @return List<List<T>> 反馈的List集合
     */
    public static <T> List<List<T>> listSpilt(List<T> list, int offset){
        List result = new ArrayList();
        int lastIndex = list.size()-1;
        for (int i = 0; i <=lastIndex; i=i+offset) {
            if ((lastIndex-i)<offset){
                result.add(list.subList(i,list.size()));
            }else {
                result.add(list.subList(i,i+offset));
            }
        }
        return result;
    }

}
