package com.hnblc.blcwms.persistent.interfaces.logistics.service.impl;

import com.hnblc.blcwms.persistent.interfaces.logistics.entity.LogisticBranchInfo;
import com.hnblc.blcwms.persistent.interfaces.logistics.mapper.LogisticBranchInfoMapper;
import com.hnblc.blcwms.persistent.interfaces.logistics.service.ILogisticBranchInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用于请求快递公司运单号使用的 发货人发货网点信息（前期主要用于拼多多专用网点） 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
@Service
public class LogisticBranchInfoServiceImpl extends ServiceImpl<LogisticBranchInfoMapper, LogisticBranchInfo> implements ILogisticBranchInfoService {

}
