package com.hnblc.blcwms.rest.dto.restInteraction;

import com.hnblc.blcwms.serviceapi.api.group.custom.express.ExpGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class ReadableRequest<T> extends BaseRequest {


    @ApiModelProperty(name = "业务操作数据",position = 20)
    @NotNull(message = "{validation.request.commonField.notEmpty}",groups = {ExpGroup.class})
    @Valid
    private T businessData;
}
