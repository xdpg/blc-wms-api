package com.hnblc.blcwms.serviceapi.api.enums.result.definition;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum CompanyResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","保存成功","SUCCESS"),
    COMPANY_EXISTED(false,"11","该运营单位已存在。","COMPANY_EXISTED"),
    UNKNOWN_OWNER_NO(false,"12","货主代码不存在","UNKNOWN_OWNER_NO"),
    WRONG_COMPANY_TYPE(false,"13","企业类型错误","WRONG_COMPANY_TYPE"),
    SHEET_EXISTED(false,"14","基础资料创建单据已存在缓冲区，请等待处理。","SHEET_EXISTED"),
    SAVE_FAIL(false,"13","保存运营单位未知错误：{0}","SAVE_FAIL");



    private boolean success;
    private String code;
    private String category="12";
    private String textCn;
    private String testEn;

    CompanyResultEnum(boolean success, String code, String textCn, String testEn){
        this.success = success;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess() {
        return this.success;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (CompanyResultEnum enums : EnumSet.allOf(CompanyResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
