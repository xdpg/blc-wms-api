package com.hnblc.blcwms.persistent.interfaces.sysConfig.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 接口系统字段字典表
 * </p>
 *
 * @author linsong
 * @since 2019-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_SYS_FIELD_DICTIONARY")
public class FieldDictionary implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一主键。
     */
    @TableId("SEQ_NO")
    private String seqNo;

    /**
     * 归属表名。
     */
    @TableField("TABLE_NAME")
    private String tableName;

    /**
     * 归属字段。
     */
    @TableField("TABLE_COLUMN")
    private String tableColumn;

    /**
     * 字段值代码。
     */
    @TableField("COLUMN_VALUE")
    private String columnValue;

    /**
     * 字段值文字。
     */
    @TableField("COLUMN_TEXT")
    private String columnText;

    /**
     * 字段值中文。
     */
    @TableField("COLUMN_TEXT_CN")
    private String columnTextCn;

    /**
     * 是否启用。
     */
    @TableField("ISENABLED")
    private String isenabled;

    /**
     * 字段值详细说明。
     */
    @TableField("VALUE_DESCRIPTION")
    private String valueDescription;


}
