package com.hnblc.blcwms.serviceapi.api.dto.definition;

import com.hnblc.blcwms.serviceapi.api.group.owner.definition.DefArticleGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ArticleInfo {
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 20)
    private String ownerArticleNo;
    private String article_identifier;
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 20)
    private String groupNo;
    private String supplierNo;
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 20)
    private String barcode;
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 100)
    private String articleName;
    private String ename;
    private String abcid;
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 6)
    private String unit;
    @NotNull(groups = {DefArticleGroup.class})
    @Min(1)
    private Double packingQty;
    @NotBlank(groups = {DefArticleGroup.class})
    @Length(max = 50)
    private String spec;
    private Double length;
    private Double width;
    private Double height;
    private Double unitVolume;
    private Double weight;
    private Double expiryDays;
    private Double expiryRate;
    private Double freezeRate;
    private Double price;
    private String notes;
    private String boxScanFlag;
    private String temperatureFlag;
    private String batchFlag;
    private String status;


    private String errorReason;

}
