package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsOwner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
public interface IWmsOwnerService extends IService<WmsOwner> {

}
