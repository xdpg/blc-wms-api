package com.hnblc.blcwms.persistent.business.stock.entity;


import lombok.Data;

@Data
//@TableName("articleStock")
public class ArticleStock {
    private String ownerNo;
    private String ownerName;
    private String ownerArticleNo;
    private String barcode;
    private String articleIdentifier;
    private String unit;
    private String cellNo;
    private String realQty;
    private String expQty;
    private String planQty;
    private String salableQty;
}
