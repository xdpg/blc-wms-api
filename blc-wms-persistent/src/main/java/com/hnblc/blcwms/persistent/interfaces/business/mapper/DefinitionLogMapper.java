package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.hnblc.blcwms.persistent.interfaces.business.entity.DefinitionLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 接口操作基础资料记录 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-11-28
 */
public interface DefinitionLogMapper extends BaseMapper<DefinitionLog> {

}
