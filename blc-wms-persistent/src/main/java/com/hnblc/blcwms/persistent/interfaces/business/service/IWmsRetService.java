package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-12-17
 */
public interface IWmsRetService extends IService<WmsRet> {

}
