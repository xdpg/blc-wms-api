package com.hnblc.blcwms.persistent.interfaces.client.service;

import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用于查询客户端有哪些客户的查询权限 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface IClientAuthService extends IService<ClientAuth> {

}
