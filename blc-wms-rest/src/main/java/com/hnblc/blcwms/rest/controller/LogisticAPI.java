package com.hnblc.blcwms.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hnblc.blcwms.common.exceptions.ErrorDescription;
import com.hnblc.blcwms.common.exceptions.GeneralException;
import com.hnblc.blcwms.common.interaction.RestResponse;
import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpMService;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.rest.service.impl.LogisticsApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/logistics")
public class LogisticAPI {


    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    IOdataExpMService odataExpMService;
    @Autowired
    LogisticsApplyService logisticsApplyService;
    /*@SuppressWarnings("unchecked")
    @CrossOrigin(origins = {"*"})
    @GetMapping("/apply_waybill_info")
    @ResponseBody
    public RestResponse getPDDYTOWaybillInfo(@RequestParam("expNos") String expNo, @RequestParam String warehouseNo,@RequestParam String logisticCode){
        ExpressQueryVO exp = new ExpressQueryVO();
        exp.setExpNo(expNo);
        exp.setWarehouseNo(warehouseNo);


        //加载获取运单号内容
        return new RestResponse(logisticsApplyService.applyWaybillInfo(exp,logisticCode));
    }*/

    @CrossOrigin(origins = {"*"})
    @GetMapping("/apply_waybill_info")
    @ResponseBody
    public String getPDDYTOWaybillInfo(@RequestParam("expNos") String expNo, @RequestParam String warehouseNo,@RequestParam String logisticCode,String callback){
        ExpressQueryVO exp = new ExpressQueryVO();
        exp.setExpNo(expNo);
        exp.setWarehouseNo(warehouseNo);

        try {
            return callback+"("+objectMapper.writeValueAsString(new RestResponse(logisticsApplyService.applyWaybillInfo(exp,logisticCode)))+")";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            try {
                return callback+"("+objectMapper.writeValueAsString(new RestResponse(new GeneralException(ErrorDescription.ERROR_REQUEST_INTERNAL_EXCEPTION_0)))+")";
            } catch (JsonProcessingException e1) {
                e1.printStackTrace();
                return "error";
            }
        }
    }
}
