package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_OWNER")
public class WmsOwner implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("SHEETID")
    private String sheetid;

    /**
     * 企业编码
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 货主编码
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    @TableField("OWNER_NAME")
    private String ownerName;

    /**
     * 货主简称
     */
    @TableField("OWNER_ALIAS")
    private String ownerAlias;

    /**
     * 货主地址
     */
    @TableField("OWNER_ADDRESS")
    private String ownerAddress;

    /**
     * 货主传真
     */
    @TableField("OWNER_FAX")
    private String ownerFax;

    /**
     * 货主电话
     */
    @TableField("OWNER_PHONE")
    private String ownerPhone;

    /**
     * 货主联系人
     */
    @TableField("OWNER_CONTACT")
    private String ownerContact;

    @TableField("STATUS")
    private Double status;

    @TableField("NOTES")
    private String notes;

    /**
     * 发票号
     */
    @TableField("INVOICE_NO")
    private String invoiceNo;

    /**
     * 发票地址
     */
    @TableField("INVOICE_ADDR")
    private String invoiceAddr;

    /**
     * 发票抬头
     */
    @TableField("INVOICE_HEADER")
    private String invoiceHeader;

    /**
     * 报关号
     */
    @TableField("RSV_VAR2")
    private String rsvVar2;

    /**
     * 账册
     */
    @TableField("RSV_VAR3")
    private String rsvVar3;

    @TableField("HANDLETIME")
    private LocalDateTime handletime;


}
