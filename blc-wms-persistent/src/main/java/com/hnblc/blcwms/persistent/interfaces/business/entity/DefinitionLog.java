package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hnblc.blcwms.common.enums.ResultEnum;
import com.hnblc.blcwms.common.interaction.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 接口操作基础资料记录
 * </p>
 *
 * @author linsong
 * @since 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_B_DEFINITION_LOG")
public class DefinitionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一主键。
     */
    @TableId("SEQ_NO")
    private String seqNo;


    /**
     * 创建基础资料的唯一编号，对应各接口业务表中的sheetid
     */
    @TableField("DEFINITION_NO")
    private String definitionNo;

    /**
     * 操作客户端
     */
    @TableField("OPERATE_CLIENT")
    private String operateClient;

    /**
     * 操作类型,1创建货主，2，创建客户，3创建供应商，4创建商品类别，5创建商品基础资料
     */
    @TableField("DEFINITION_TYPE")
    private String definitionType;

    /**
     * 操作成功失败
     */
    @TableField("STATUS")
    private String status;

    /**
     * 失败原因代码代码
     */
    @TableField("ERROR_CODE")
    private String errorCode;

    /**
     * 操作日期
     */
    @TableField("OPERATE_DATE")
    private LocalDateTime operateDate;

    /**
     * 原始报文ID
     */
    @TableField("MESSAGE_SEQ")
    private String messageSeq;

    public DefinitionLog(){}

    public DefinitionLog(String definitionType, String operateClient, String messageSeq, String definitionNo, Response result){
        this.definitionType = definitionType;
        this.operateClient = operateClient;
        this.messageSeq = messageSeq;
        this.definitionNo =  definitionNo;
        this.operateDate = LocalDateTime.now();
        this.errorCode = result.getDicCode();
        if (result.isSuccess()){
            this.status = ResultEnum.SUCCESS.code();
        }else {
            this.status = ResultEnum.FAILURE.code();
        }
    }
}
