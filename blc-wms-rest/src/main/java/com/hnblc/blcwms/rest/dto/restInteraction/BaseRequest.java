package com.hnblc.blcwms.rest.dto.restInteraction;


import com.hnblc.blcwms.common.validation.annotation.NotOverTime;
import com.hnblc.blcwms.serviceapi.api.group.BaseGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class BaseRequest {
    @ApiModelProperty(name = "客户端ID",example = "424b51ee165c4a588533847e2346d9fd",position = 1)
    @NotBlank(groups = {BaseGroup.class})
    private String clientId;
    @ApiModelProperty(name = "当前请求时间戳",example = "1959294916451",position = 2)
    @NotOverTime(maxOverTime = 300000,message="{validation.request.commonField.notOverTime}",groups = {BaseGroup.class})
    private String timestamp;
    @ApiModelProperty(name = "接口版本号",example = "1.0",position = 4)
    @NotBlank(groups = {BaseGroup.class})
    private String version;
}
