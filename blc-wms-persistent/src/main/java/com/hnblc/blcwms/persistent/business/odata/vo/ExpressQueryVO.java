package com.hnblc.blcwms.persistent.business.odata.vo;

import com.hnblc.blcwms.common.constant.StringPool;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ExpressQueryVO {
    private String enterpriseNo;
    private String warehouseNo;
    private String expNo;
    private List<String> expNos;
    private String ownerNo;
    private String custNo;
    private String sourceexpNo;
    private List<String> sourceexpNos;
    private String shipperDeliverNo;
    private List<String> shipperDeliverNos;
    private String deliverAddress;
    private String expType;
    private String status;
    private String expStatus;
    private String articleNo;
    private String articleName;
    private String ownerArticleNo;
    private String rsv_varod5;
    private String rsv_varod6;
    private String rsv_varod7;
    private String rsv_varod8;


    public void setExpNo(String expNo){
        if (expNo.contains(StringPool.COMMON_SEPARATOR_SPLIT_COMMA)){
            this.setExpNos(new ArrayList<>(Arrays.stream(expNo.split(StringPool.COMMON_SEPARATOR_SPLIT_COMMA)).collect(Collectors.toList())));
        }
        else
            this.expNo = expNo;
    }
}
